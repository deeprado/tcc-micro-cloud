package com.seata.ali.account;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 */
@SpringBootApplication
public class AliAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliAccountApplication.class, args);
    }

}