package com.seata.ali.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.seata.ali.db.entity.Account;
import com.seata.ali.db.mapper.AccountMapper;
import com.seata.ali.db.service.IAccountService;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

}
