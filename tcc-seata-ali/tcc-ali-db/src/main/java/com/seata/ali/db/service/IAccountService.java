package com.seata.ali.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.ali.db.entity.Account;

/**
 *
 * @author deeprado
 */
public interface IAccountService extends IService<Account> {

}
