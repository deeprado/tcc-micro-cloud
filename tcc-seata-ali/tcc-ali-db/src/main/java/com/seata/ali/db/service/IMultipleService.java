package com.seata.ali.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.ali.db.entity.Orders;

public interface IMultipleService extends IService<Orders> {
}
