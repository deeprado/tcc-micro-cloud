package com.seata.ali.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.seata.ali.db.entity.Orders;
import com.seata.ali.db.mapper.OrdersMapper;
import com.seata.ali.db.service.IOrdersService;
import org.springframework.stereotype.Service;

@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements IOrdersService {

}
