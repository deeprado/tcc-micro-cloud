package com.seata.ali.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.ali.db.entity.Orders;


/**
 *
 * @author deeprado
 * @since 2019-12-07
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
