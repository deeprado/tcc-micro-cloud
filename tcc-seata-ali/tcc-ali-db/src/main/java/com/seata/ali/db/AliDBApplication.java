package com.seata.ali.db;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author deeprado
 */
@SpringBootApplication
public class AliDBApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliDBApplication.class, args);
    }

}