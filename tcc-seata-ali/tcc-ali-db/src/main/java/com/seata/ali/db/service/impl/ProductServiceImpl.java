package com.seata.ali.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.seata.ali.db.entity.Product;
import com.seata.ali.db.mapper.ProductMapper;
import com.seata.ali.db.service.IProductService;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

}
