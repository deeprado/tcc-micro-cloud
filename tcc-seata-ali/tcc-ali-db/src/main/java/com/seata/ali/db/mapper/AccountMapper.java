package com.seata.ali.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.ali.db.entity.Account;

/**
 *
 * @author deeprado
 */
public interface AccountMapper extends BaseMapper<Account> {

}
