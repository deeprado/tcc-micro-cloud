package com.seata.ali.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author deeprado
 */
@SpringBootApplication(scanBasePackages = "com.seata.ali.db, com.seata.ali.provider")
public class AliProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliProviderApplication.class, args);
    }

}