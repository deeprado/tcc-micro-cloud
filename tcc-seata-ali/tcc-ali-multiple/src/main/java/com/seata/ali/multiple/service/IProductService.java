package com.seata.ali.multiple.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.ali.multiple.entity.Product;


/**
 * <p>
 * 功能 服务类
 * </p>
 *
 * @author deeprado
 * @since 2019-04-10
 */
public interface IProductService extends IService<Product> {

}
