package com.seata.ali.multiple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author deeprado
 * @date 2019/12/7
 */
@SpringBootApplication
public class MultipleApplication {
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(MultipleApplication.class);
        app.run(args);
    }

}
