package com.seata.ali.multiple.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.ali.multiple.entity.Account;

/**
 *
 * @author deeprado
 * @since 2019-12-07
 */
public interface AccountMapper extends BaseMapper<Account> {

}
