package com.seata.ali.multiple.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seata.ali.multiple.entity.Orders;
import com.seata.ali.multiple.mapper.OrdersMapper;
import com.seata.ali.multiple.service.IOrdersService;
import org.springframework.stereotype.Service;

@DS(value = "master_2")
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements IOrdersService {

}
