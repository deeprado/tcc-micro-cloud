package com.seata.ali.multiple.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.ali.multiple.entity.Product;

/**
 *
 * @author deeprado
 * @since 2019-12-07
 */
public interface ProductMapper extends BaseMapper<Product> {

}
