package com.seata.ali.multiple.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seata.ali.multiple.entity.Product;
import com.seata.ali.multiple.mapper.ProductMapper;
import com.seata.ali.multiple.service.IProductService;
import org.springframework.stereotype.Service;

@DS(value = "master_3")
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

}
