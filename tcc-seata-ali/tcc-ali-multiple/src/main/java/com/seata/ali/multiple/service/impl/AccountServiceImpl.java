package com.seata.ali.multiple.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;


import com.seata.ali.multiple.entity.Account;
import com.seata.ali.multiple.mapper.AccountMapper;
import com.seata.ali.multiple.service.IAccountService;
import org.springframework.stereotype.Service;

@DS(value = "master_1")
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

}
