package com.seata.ali.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author deeprado
 */
@SpringBootApplication
public class AliOrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(AliOrderApplication.class, args);
    }

}