package com.seata.order.service;

import com.seata.core.exception.BusinessException;

public interface StorageService {

    boolean decrease(String businessKey, Long productId, Integer count) throws BusinessException;

    /**
     * 补偿扣减库存
     * @param businessKey businessKey
     * @param productId productId
     * @param count count
     * @return
     */
    boolean compensateDecrease(String businessKey, Long productId, Integer count) throws BusinessException;
}
