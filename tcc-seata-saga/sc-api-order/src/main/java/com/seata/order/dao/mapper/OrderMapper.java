package com.seata.order.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.order.dao.entity.Order;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;

/**
 * @author deeprado
 */
@Repository
public interface OrderMapper extends BaseMapper<Order> {

    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    @Insert("INSERT INTO `orders` (`id`,`user_id`,`product_id`,`count`,`pay_amount`,`status`) VALUES(NULL, #{userId}, #{productId}, #{count}, #{payAmount},0)")
    void create(Order order);

    /**
     * 修改订单金额
     *
     * @param userId
     * @param payAmount
     */
    @Update("UPDATE `orders` SET pay_amount = pay_amount - #{payAmount},status = 1 where user_id = #{userId} and status = #{status};")
    void update(@Param("userId") Long userId, @Param("payAmount") BigDecimal payAmount, @Param("status") Integer status);

    /**
     * 删除订单
     *
     * @param order
     */
    @Delete("delete from orders where id=#{id}")
    void delete(Order order);
}
