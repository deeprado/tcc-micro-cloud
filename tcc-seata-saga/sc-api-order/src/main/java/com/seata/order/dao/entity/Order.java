package com.seata.order.dao.entity;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 订单
 * @author deeprado
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Order", description = "订单信息")
public class Order {

    @ApiModelProperty(name = "用户id", dataType = "Long")
    private Long id;

    @ApiModelProperty(name = "userId", dataType = "Long")
    private Long userId;

    @ApiModelProperty(name = "商品id", dataType = "Long")
    private Long productId;

    @ApiModelProperty(name = "购买数量", dataType = "Long")
    private Integer count;

    @ApiModelProperty(name = "订单金额", dataType = "Long")
    private BigDecimal payAmount;

    /**订单状态：0：创建中；1：已完结*/
    private Integer status;

}
