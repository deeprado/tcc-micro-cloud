package com.seata.order.controller;

import com.seata.order.dao.entity.Order;
import com.seata.order.service.OrderApiService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author deeprado
 */
@RestController
@RequestMapping(value = "/order")
@Api(tags = {"创建订单的控制层"}, description = "描述")
public class OrderController {

    @Resource
    private OrderApiService orderApiService;

    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    @PostMapping("/create")
    @ApiOperation(value = "创建订单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order", value = "order", required = true, dataType = "Order")
    })
    @ApiResponse(code = 200, message = "message", response = String.class)
    public String create(@RequestBody Order order) {
        if (orderApiService.saveOrder(order)) {
            return "Create order success";
        }
        return "Create order fail";
    }

    @GetMapping("/test1")
    @ApiOperation(value = "创建订单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order", value = "order", required = true, dataType = "Order")
    })
    @ApiResponse(code = 200, message = "message", response = String.class)
    public String test1() {
        Order order = new Order(
                null,
                1L,
                1L,
                1,
                new BigDecimal(20),
                1
        );

        if (orderApiService.saveOrder(order)) {
            return "Create order success";
        }
        return "Create order fail";
    }

    @GetMapping("/test2")
    @ApiOperation(value = "创建订单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order", value = "order", required = true, dataType = "Order")
    })
    @ApiResponse(code = 200, message = "message", response = String.class)
    public String test2() {
        Order order = new Order(
                null,
                1L,
                1L,
                3,  // 库存异常
                new BigDecimal(60),
                1
        );

        if (orderApiService.saveOrderErr(order)) {
            return "Create order success";
        }
        return "Create order fail";
    }
    @GetMapping("/test3")
    @ApiOperation(value = "创建订单", httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "order", value = "order", required = true, dataType = "Order")
    })
    @ApiResponse(code = 200, message = "message", response = String.class)
    public String test3() {
        Order order = new Order(
                null,
                1L,
                1L,
                2,  //
                new BigDecimal(40), // 账户异常
                1
        );

        if (orderApiService.saveOrderErr(order)) {
            return "Create order success";
        }
        return "Create order fail";
    }

}
