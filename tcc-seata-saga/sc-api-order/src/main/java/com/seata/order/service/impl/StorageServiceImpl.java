package com.seata.order.service.impl;

import com.seata.core.exception.BusinessException;
import com.seata.core.support.LogSupport;
import com.seata.order.feign.StorageFeignClient;
import com.seata.order.service.StorageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("storageService")
public class StorageServiceImpl implements StorageService {

    @Resource
    private StorageFeignClient storageFeignClient;

    @Override
    public boolean decrease(String businessKey, Long productId, Integer count) throws BusinessException {
        try {
            return storageFeignClient.decrease(businessKey, productId, count);
        } catch (Exception e) {
            LogSupport.logError(e);
            throw new BusinessException("库存扣除失败");
        }
    }

    @Override
    public boolean compensateDecrease(String businessKey, Long productId, Integer count) throws BusinessException {
        try {
            return storageFeignClient.compensateDecrease(businessKey, productId, count);
        } catch (Exception e) {
            LogSupport.logError(e);
            throw new BusinessException("库存补偿失败");
        }
    }
}
