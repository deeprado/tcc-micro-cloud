package com.seata.order.service;

import com.seata.order.dao.entity.Order;

public interface OrderApiService {

    /**
     * <p>
     * 创建订单
     * </p>
     *
     * @param order 订单信息
     * @return boolean
     * @author deeprado
     * @date 2021/10/29 12:00 上午
     * @since 1.0
     */
    boolean saveOrder(Order order);

    /**
     *
     * @param order
     * @return
     */
    boolean saveOrderErr(Order order);
}
