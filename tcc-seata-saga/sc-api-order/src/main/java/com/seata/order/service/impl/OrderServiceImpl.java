package com.seata.order.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seata.core.support.LogSupport;
import com.seata.order.dao.entity.Order;
import com.seata.order.dao.mapper.OrderMapper;
import com.seata.order.service.OrderService;
import org.springframework.stereotype.Service;

@Service("orderService")
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Override
    public boolean saveOrder(String businessKey, Order order) {
        LogSupport.logInfo("保存订单, businessKey：{}, order: {}", businessKey, order);
        getBaseMapper().create(order);
        return true;
    }

    /**
     * 回滚事务,删除订单
     *
     * @param order order
     * @return
     */
    @Override
    public boolean compensateDecrease(String businessKey, Order order) {
        LogSupport.logInfo("删除订单, businessKey：{}, order: {}", businessKey, order);
        getBaseMapper().delete(order);
        return true;
    }
}
