package com.seata.order.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author deeprado
 */
@FeignClient(value = "sc-api-storage")
public interface StorageFeignClient {

    /**
     * 扣减库存
     *
     * @param businessKey businessKey
     * @param productId   productId
     * @param count       count
     * @returns
     */
    @GetMapping(value = "/storage/decrease")
    boolean decrease(@RequestParam("businessKey") String businessKey,
                     @RequestParam("productId") Long productId,
                     @RequestParam("count") Integer count);

    /**
     * 补偿扣减库存
     * @param businessKey businessKey
     * @param productId productId
     * @param count count
     * @return
     */
    @GetMapping(value = "/storage/compensateDecrease")
    boolean compensateDecrease(@RequestParam("businessKey") String businessKey,
                               @RequestParam("productId") Long productId,
                               @RequestParam("count") Integer count);

}
