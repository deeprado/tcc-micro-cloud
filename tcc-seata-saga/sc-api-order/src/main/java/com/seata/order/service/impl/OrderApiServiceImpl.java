package com.seata.order.service.impl;

import com.seata.core.support.LogSupport;
import com.seata.order.dao.entity.Order;
import com.seata.order.service.OrderApiService;
import com.seata.order.util.ApplicationContextUtils;
import io.seata.saga.engine.StateMachineEngine;
import io.seata.saga.statelang.domain.ExecutionStatus;
import io.seata.saga.statelang.domain.StateMachineInstance;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;


@Service("orderApiService")
public class OrderApiServiceImpl implements OrderApiService {

    @Override
    public boolean saveOrder(Order order) {
        LogSupport.logInfo("------->交易开始");
        StateMachineEngine stateMachineEngine = (StateMachineEngine) ApplicationContextUtils
                .getApplicationContext().getBean("stateMachineEngine");

        Map<String, Object> startParams = new HashMap<>(16);
        String businessKey = String.valueOf(System.currentTimeMillis());
        startParams.put("businessKey", businessKey);
        startParams.put("order", order);
        startParams.put("mockReduceAccountFail", "true");
        startParams.put("userId", order.getUserId());
        startParams.put("money", order.getPayAmount());
        startParams.put("productId", order.getProductId());
        startParams.put("count", order.getCount());

        //sync test
        StateMachineInstance inst = stateMachineEngine.startWithBusinessKey("buyGoodsOnline", null, businessKey, startParams);
        if (ExecutionStatus.SU.equals(inst.getStatus())) {
            LogSupport.logInfo("saga transaction commit succeed. XID: {}", inst.getId());
            inst = stateMachineEngine.getStateMachineConfig().getStateLogStore().getStateMachineInstanceByBusinessKey(businessKey, null);
            if (ExecutionStatus.SU.equals(inst.getStatus())) {
                return true;
            }
            LogSupport.logInfo("saga transaction execute failed. XID:{},status:{}", inst.getId(), inst.getStatus());
            return false;
        }
        return false;
    }

    @Override
    public boolean saveOrderErr(Order order) {
        LogSupport.logInfo("------->交易开始");
        StateMachineEngine stateMachineEngine = (StateMachineEngine) ApplicationContextUtils
                .getApplicationContext().getBean("stateMachineEngine");

        Map<String, Object> startParams = new HashMap<>(16);
        String businessKey = String.valueOf(System.currentTimeMillis());
        startParams.put("businessKey", businessKey);
        startParams.put("order", order);
        startParams.put("mockReduceAccountFail", "true");
        startParams.put("userId", order.getUserId());
        startParams.put("money", order.getPayAmount());
        startParams.put("productId", order.getProductId());
        startParams.put("count", order.getCount());

        //sync test
        StateMachineInstance inst = stateMachineEngine.startWithBusinessKey("buyGoodsOnline", null, businessKey, startParams);
        if (ExecutionStatus.SU.equals(inst.getStatus())) {
            LogSupport.logInfo("saga transaction commit succeed. XID: {}", inst.getId());
            inst = stateMachineEngine.getStateMachineConfig().getStateLogStore().getStateMachineInstanceByBusinessKey(businessKey, null);
            if (ExecutionStatus.SU.equals(inst.getStatus())) {
                return true;
            }
            LogSupport.logInfo("saga transaction execute failed. XID:{},status:{}", inst.getId(), inst.getStatus());
            return false;
        }
        return false;
    }
}
