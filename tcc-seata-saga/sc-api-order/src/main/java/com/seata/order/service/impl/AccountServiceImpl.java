package com.seata.order.service.impl;

import com.seata.core.exception.BusinessException;
import com.seata.core.support.LogSupport;
import com.seata.order.feign.AccountFeignClient;
import com.seata.order.service.AccountService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

@Service("accountService")
public class AccountServiceImpl implements AccountService {

    @Resource
    private AccountFeignClient accountFeignClient;

    @Override
    public boolean decrease(String businessKey, Long userId, BigDecimal money) throws BusinessException {
        try {
            return accountFeignClient.decrease(businessKey, userId, money);
        } catch (Exception e) {
            LogSupport.logError(e);
            throw new BusinessException("账户资金扣除失败");
        }
    }

    @Override
    public boolean compensateDecrease(String businessKey, Long userId, BigDecimal money) throws BusinessException {
        try {
            return accountFeignClient.compensateDecrease(businessKey, userId, money);
        } catch (Exception e) {
            LogSupport.logError(e);
            throw new BusinessException("账户资金补偿失败");
        }
    }
}
