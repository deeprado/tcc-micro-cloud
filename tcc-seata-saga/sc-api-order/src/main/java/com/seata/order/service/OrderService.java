package com.seata.order.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.order.dao.entity.Order;

public interface OrderService extends IService<Order> {

    boolean saveOrder(String businessKey, Order order);

    /**
     * 失败补偿，删除订单
     *
     * @param order order
     * @return
     */
    boolean compensateDecrease(String businessKey, Order order);
}
