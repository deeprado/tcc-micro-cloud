package com.seata.storage.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.core.exception.BusinessException;
import com.seata.storage.dao.entity.Storage;

/**
 *
 */
public interface StorageService extends IService<Storage> {

    /**
     * 扣减库存
     * @param productId 产品id
     * @param count 数量
     * @return
     */
    boolean decrease(Long productId, Integer count) throws BusinessException, InterruptedException;

    /**
     * 回滚扣减库存
     * @param productId 产品id
     * @param count 数量
     * @return
     */
    boolean compensateDecrease(Long productId, Integer count) throws BusinessException;
}
