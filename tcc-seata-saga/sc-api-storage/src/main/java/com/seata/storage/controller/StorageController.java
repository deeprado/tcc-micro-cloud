package com.seata.storage.controller;

import com.seata.core.exception.BusinessException;
import com.seata.core.support.LogSupport;
import com.seata.storage.service.StorageService;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 *
 */
@RestController
@RequestMapping("/storage")
@Api(tags = {"库存的控制层"}, description = "描述")
public class StorageController {

    @Resource
    private StorageService storageService;

    /**
     * 扣减库存
     *
     * @param productId 产品id
     * @param count     数量
     * @return 扣减库存结果
     */
    @GetMapping("/decrease")
    @ApiOperation(value = "扣减库存", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "businessKey", value = "自定义业务id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "productId", value = "商品id", required = true, dataType = "long"),
            @ApiImplicitParam(name = "count", value = "数量", required = true, dataType = "int")
    })
    @ApiResponse(code = 200, message = "message", response = Boolean.class)
    public boolean decrease(@RequestParam("businessKey") String businessKey,
                            @RequestParam("productId") Long productId,
                            @RequestParam("count") Integer count) throws BusinessException, InterruptedException {
        LogSupport.logInfo("businessKey:{}", businessKey);
        return storageService.decrease(productId, count);
    }

    /**
     * 补偿扣减库存
     *
     * @param businessKey businessKey
     * @param productId   产品id
     * @param count       数量
     * @return 补偿结果
     */
    @GetMapping("compensateDecrease")
    @ApiOperation(value = "补偿扣减库存", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "businessKey", value = "自定义业务id", required = true, dataType = "String"),
            @ApiImplicitParam(name = "productId", value = "productId", required = true, dataType = "long"),
            @ApiImplicitParam(name = "count", value = "count", required = true, dataType = "int")
    })
    @ApiResponse(code = 200, message = "message", response = Boolean.class)
    public boolean compensateDecrease(@RequestParam("businessKey") String businessKey,
                                      @RequestParam("productId") Long productId,
                                      @RequestParam("count") Integer count) throws BusinessException {
        LogSupport.logInfo("businessKey:{}", businessKey);
        return storageService.compensateDecrease(productId, count);
    }
}
