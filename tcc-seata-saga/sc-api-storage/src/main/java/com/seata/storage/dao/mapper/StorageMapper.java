package com.seata.storage.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.storage.dao.entity.Storage;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

/**
 *
 */
@Repository
public interface StorageMapper extends BaseMapper<Storage> {

    /**
     * 扣减库存
     *
     * @param productId 产品id
     * @param count     数量
     * @return
     */
    @Update("UPDATE storage SET used = used + #{count},residue = residue - #{count} WHERE product_id = #{productId}")
    void decrease(@Param("productId") Long productId, @Param("count") Integer count);

    /**
     * 补偿扣减库存
     *
     * @param productId 产品id
     * @param count     数量
     * @return
     */
    @Update("UPDATE storage SET used = used - #{count},residue = residue + #{count} WHERE product_id = #{productId}")
    void compensateDecrease(@Param("productId") Long productId, @Param("count") Integer count);
}
