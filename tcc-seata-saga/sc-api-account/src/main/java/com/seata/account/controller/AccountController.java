package com.seata.account.controller;

import com.seata.account.service.AccountService;
import com.seata.core.exception.BusinessException;
import com.seata.core.support.LogSupport;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @author deeprado
 */
@RestController
@RequestMapping("/account")
@Api(tags = {"账户消费的控制层"}, description = "描述")
public class AccountController {
    @Resource
    private AccountService accountServiceImpl;


    /**
     * <p>
     * 余额消费
     * </p>
     *
     * @param userId 用户id
     * @param money  金额
     * @return boolean
     * @author deeprado
     * @date 2021/10/29 12:14 上午
     * @since 1.0
     */
    @GetMapping("/decrease")
    @ApiOperation(value = "余额消费", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "money", value = "money", required = true, dataType = "double"),
            @ApiImplicitParam(name = "userId", value = "userId", required = true, dataType = "Long")
    })
    @ApiResponse(code = 200, message = "message", response = Boolean.class)
    public boolean decrease(
            @RequestParam("businessKey") String businessKey,
            @RequestParam("userId") Long userId,
            @RequestParam("money") BigDecimal money) throws BusinessException, InterruptedException {
        LogSupport.logInfo("businessKey:{}", businessKey);
        return accountServiceImpl.decrease(userId, money);
    }

    @ApiOperation(value = "余额消费补偿", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "money", value = "money", required = true, dataType = "double"),
            @ApiImplicitParam(name = "userId", value = "userId", required = true, dataType = "Long")
    })
    @ApiResponse(code = 200, message = "message", response = Boolean.class)
    @RequestMapping("compensateDecrease")
    public boolean compensateDecrease(
            @RequestParam("businessKey") String businessKey,
            @RequestParam("userId") Long userId,
            @RequestParam("money") BigDecimal money) throws BusinessException {
        LogSupport.logInfo("businessKey:{}", businessKey);
        return accountServiceImpl.compensateDecrease(userId, money);
    }
}
