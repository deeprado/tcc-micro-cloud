package com.seata.account.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.account.dao.entity.Account;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;

import java.math.BigDecimal;

/**
 * <p>
 * Account Mapper接口
 * </p>
 *
 * @author deeprado
 * @date 2021/10/28 8:47 下午
 * @since 1.0
 */
public interface AccountMapper extends BaseMapper<Account> {

    /**
     * 扣减账户余额
     * @param userId 用户id
     * @param payAmount 金额
     */
    @Update("UPDATE account SET balance = balance - #{payAmount},used = used + #{payAmount} where user_id = #{userId}")
    void decrease(@Param("userId") Long userId, @Param("payAmount") BigDecimal payAmount);
}
