package com.seata.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.account.dao.entity.Account;
import com.seata.core.exception.BusinessException;

import java.math.BigDecimal;

/**
 * <p>
 * 账户接口
 * </p>
 *
 * @author deeprado
 * @date 2021/10/28 8:50 下午
 * @since 1.0
 */
public interface AccountService extends IService<Account> {

    /**
     * 扣减账户余额
     * @param userId 用户id
     * @param money 金额
     * @return prepare是否成功
     */
    boolean decrease(Long userId, BigDecimal money) throws BusinessException, InterruptedException;

    /**
     * 补偿账户余额
     *
     * @param userId 用户id
     * @param money 金额
     * @return the boolean
     */
    boolean compensateDecrease(Long userId, BigDecimal money) throws BusinessException;
}
