package com.seata.core.support;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogSupport {
    private static final Logger logger = LoggerFactory.getLogger(LogSupport.class);

    public static void logInfo(String format, Object... arguments) {
        //
        logger.info(format, arguments);
    }

    public static void logInfo(String format) {
        //
        logger.info(format);
    }

    public static void logError(Exception e) {
        //
        logger.error("e = {}", e);
    }


}
