




1. nacos
   https://blog.csdn.net/qq853632587/article/details/111644295

```shell

sh nacos-config.sh -h localhost -p 8848 -g DEFAULT_GROUP -t b47b8b25-4d6a-4beb-9836-5559a9a93b1a -u nacos -w nacos
sh nacos-config.sh -h localhost -p 8848 -g SEATA_GROUP -t 40508bb4-179e-4c98-a2f1-c2c031c20b3c -u nacos -w nacos
sh nacos-config.sh -h localhost -p 8848 -g SEATA_GROUP -t b47b8b25-4d6a-4beb-9836-5559a9a93b1a -u nacos -w nacos
sh nacos-config.sh -h localhost -p 8848 -g SEATA_GROUP -t 2abdf698-927c-4f9e-9159-b777d7d5b0f9 -u nacos -w nacos

 sh ${SEATAPATH}/script/config-center/nacos/nacos-config.sh -h localhost -p 8848 -g SEATA_GROUP -t 5a3c7d6c-f497-4d68-a71a-2e5e3340b3ca -u username -w password
```

2. consul
   https://blog.csdn.net/gsh6022/article/details/110038447

```shell

sh {PATH}/consul-config.sh -h localhost -p 8500


```

3. zookeeper 

```shell

zk-config.sh -h localhost -p 2181 -z "/Users/zhangchenghui/zookeeper-3.4.14"
zk-config.sh -h localhost -p 2181 -z "D:\Serv\zookeeper\apache-zookeeper-3.8.0-bin\apache-zookeeper-3.8.0-bin"
zk-config.sh -h localhost -p 2181 -z "E:\Serv\Zookeeper\zookeeper-3.4.13"

```

4. etcd3
   https://blog.huati365.com/8d1b11c5d3370697
```shell
etcd
sh etcd3-config.sh
sh ${SEATAPATH}/script/config-center/etcd3/etcd3-config.sh -h localhost -p 2379
```
5. apollo

```shell

sh ${SEATAPATH}/script/config-center/apollo/apollo-config.sh -h localhost -p 8070 -e DEV -a seata-server -c default -n application -d apollo -r apollo -t 3d218a3b4d5f2dadde6e3d002b3ce5d66852860d
sh apollo-config.sh -h localhost -p 8070 -e DEV -a seata-server -c default -n application -d apollo -r apollo -t 3d218a3b4d5f2dadde6e3d002b3ce5d66852860d



curl -v -X POST -H "content-type:application/json;charset=UTF-8" -H "Authorization: 3d218a3b4d5f2dadde6e3d002b3ce5d66852860d" -d "{\"key\":\"bbb\",\"value\":\"ccc\",\"comment\":\"\",\"dataChangeCreatedBy\":\"apollo\"}" 'http://127.0.0.1:8070/openapi/v1/envs/DEV/apps/seata-server/clusters/default/namespaces/application/items' 


```