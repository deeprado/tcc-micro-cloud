package com.seata.business.controller;

import com.seata.business.TestDatas;
import com.seata.business.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.seata.business.service.BusinessService.SUCCESS;

@RestController
public class BusinessController {

    @Autowired
    private BusinessService businessService;

    @RequestMapping(value = "/purchase", method = RequestMethod.GET)
    public String purchase(@RequestParam(defaultValue = "0") int rollback, @RequestParam(defaultValue = "1") Integer count) {
        int orderCount = 30;
        if (count != null) {
            orderCount = count;
        }
        try {
            businessService.purchase(TestDatas.USER_ID, TestDatas.COMMODITY_CODE, orderCount,
                    rollback == 1);
        } catch (Exception exx) {
            System.out.println(exx);
            return "Purchase Failed:" + exx.getMessage();
        }
        return SUCCESS;
    }

    @RequestMapping(value = "/purchase2", method = RequestMethod.GET)
    public String purchase2(@RequestParam(defaultValue = "0") int rollback, @RequestParam(defaultValue = "1") Integer count) {
        int orderCount = 30;
        if (count != null) {
            orderCount = count;
        }

        businessService.purchase(TestDatas.USER_ID, TestDatas.COMMODITY_CODE, orderCount,
                rollback == 1);

        return SUCCESS;
    }
}
