package com.sdd.tcc.account.dubbo;

import com.sdd.tcc.account.service.ITAccountService;
import com.sdd.tcc.common.dto.AccountDTO;
import com.sdd.tcc.common.dubbo.AccountDubboService;
import io.seata.core.context.RootContext;
import com.sdd.tcc.common.response.ObjectResponse;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: deeprado
 * @Description Dubbo Api Impl
 * @Date Created in 2019/1/23 14:40
 */
@DubboService(version = "1.0.0", protocol = "${dubbo.protocol.id}",
        application = "${dubbo.application.id}", registry = "${dubbo.registry.id}", group = "${dubbo.registry.group}",
        timeout = 3000)
@Slf4j
public class AccountDubboServiceImpl implements AccountDubboService {

    @Autowired
    private ITAccountService accountService;

    @Override
    public ObjectResponse decreaseAccount(AccountDTO accountDTO) {
        log.info("全局事务id ：" + RootContext.getXID());
        return accountService.decreaseAccount(accountDTO);
    }
}
