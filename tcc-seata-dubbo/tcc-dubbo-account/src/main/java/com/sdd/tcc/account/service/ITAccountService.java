package com.sdd.tcc.account.service;

import com.baomidou.mybatisplus.service.IService;
import com.sdd.tcc.account.entity.TAccount;
import com.sdd.tcc.common.dto.AccountDTO;
import com.sdd.tcc.common.response.ObjectResponse;

/**
 * <p>
 *  服务类
 * </p>
 *
 * * @author deeprado
 * @since 2019-09-04
 */
public interface ITAccountService extends IService<TAccount> {

    /**
     * 扣用户钱
     */
    ObjectResponse decreaseAccount(AccountDTO accountDTO);
}
