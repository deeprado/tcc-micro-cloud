package com.sdd.tcc.storage.service;

import com.baomidou.mybatisplus.service.IService;
import com.sdd.tcc.common.dto.CommodityDTO;
import com.sdd.tcc.common.response.ObjectResponse;
import com.sdd.tcc.storage.entity.TStorage;

/**
 * 仓库服务
 *
 * * @author deeprado
 * @since 2019-09-04
 */
public interface ITStorageService extends IService<TStorage> {

    /**
     * 扣减库存
     */
    ObjectResponse decreaseStorage(CommodityDTO commodityDTO);
}
