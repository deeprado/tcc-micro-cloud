package com.sdd.tcc.storage.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.sdd.tcc.storage.entity.TStorage;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * * @author deeprado
 * @since 2019-09-04
 */
public interface TStorageMapper extends BaseMapper<TStorage> {

    /**
     * 扣减商品库存
     * @Param: commodityCode 商品code  count扣减数量
     * @Return:
     */
    int decreaseStorage(@Param("commodityCode") String commodityCode, @Param("count") Integer count);
}
