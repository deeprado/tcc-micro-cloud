package com.sdd.tcc.storage.service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.sdd.tcc.common.dto.CommodityDTO;
import com.sdd.tcc.common.enums.RspStatusEnum;
import com.sdd.tcc.common.response.ObjectResponse;
import com.sdd.tcc.storage.entity.TStorage;
import com.sdd.tcc.storage.mapper.TStorageMapper;

import org.springframework.stereotype.Service;

/**
 * <p>
 *  库存服务实现类
 * </p>
 *
 * * @author deeprado
 * @since 2019-09-04
 */
@Service
public class TStorageServiceImpl extends ServiceImpl<TStorageMapper, TStorage> implements ITStorageService {

    @Override
    public ObjectResponse decreaseStorage(CommodityDTO commodityDTO) {
        int storage = baseMapper.decreaseStorage(commodityDTO.getCommodityCode(), commodityDTO.getCount());
        ObjectResponse<Object> response = new ObjectResponse<>();
        if (storage > 0){
            response.setStatus(RspStatusEnum.SUCCESS.getCode());
            response.setMessage(RspStatusEnum.SUCCESS.getMessage());
            return response;
        }

        response.setStatus(RspStatusEnum.FAIL.getCode());
        response.setMessage(RspStatusEnum.FAIL.getMessage());
        return response;
    }
}
