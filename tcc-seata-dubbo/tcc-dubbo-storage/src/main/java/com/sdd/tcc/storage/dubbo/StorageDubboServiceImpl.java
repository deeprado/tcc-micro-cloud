package com.sdd.tcc.storage.dubbo;


import com.sdd.tcc.common.dto.CommodityDTO;
import com.sdd.tcc.common.dubbo.StorageDubboService;
import com.sdd.tcc.common.response.ObjectResponse;
import com.sdd.tcc.storage.service.ITStorageService;
import io.seata.core.context.RootContext;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: deeprado
 * @Description
 * @Date Created in 2019/1/23 16:13
 */
@DubboService(version = "1.0.0",protocol = "${dubbo.protocol.id}",
        application = "${dubbo.application.id}",registry = "${dubbo.registry.id}", group = "${dubbo.registry.group}",
        timeout = 3000)
@Slf4j
public class StorageDubboServiceImpl implements StorageDubboService {

    @Autowired
    private ITStorageService storageService;

    @Override
    public ObjectResponse decreaseStorage(CommodityDTO commodityDTO) {
        log.info("全局事务id ：" + RootContext.getXID());
        return storageService.decreaseStorage(commodityDTO);
    }
}
