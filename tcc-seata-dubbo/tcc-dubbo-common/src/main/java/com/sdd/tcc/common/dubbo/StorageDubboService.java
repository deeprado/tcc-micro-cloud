package com.sdd.tcc.common.dubbo;

import com.sdd.tcc.common.dto.CommodityDTO;
import com.sdd.tcc.common.response.ObjectResponse;

/**
 * @Author: deeprado
 * @Description  库存服务
 * @Date Created in 2019/9/5 16:22
 */
public interface StorageDubboService {

    /**
     * 扣减库存
     */
    ObjectResponse decreaseStorage(CommodityDTO commodityDTO);
}
