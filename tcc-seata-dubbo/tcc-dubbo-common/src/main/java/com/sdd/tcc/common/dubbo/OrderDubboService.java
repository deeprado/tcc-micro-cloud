package com.sdd.tcc.common.dubbo;

import com.sdd.tcc.common.dto.OrderDTO;
import com.sdd.tcc.common.response.ObjectResponse;

/**
 * @Author: deeprado
 * @Description  订单服务接口
 * @Date Created in 2019/9/5 16:28
 */
public interface OrderDubboService {

    /**
     * 创建订单
     */
    ObjectResponse<OrderDTO> createOrder(OrderDTO orderDTO);
}
