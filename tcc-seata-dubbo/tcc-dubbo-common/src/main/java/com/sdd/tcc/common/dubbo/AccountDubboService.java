package com.sdd.tcc.common.dubbo;

import com.sdd.tcc.common.dto.AccountDTO;
import com.sdd.tcc.common.response.ObjectResponse;

/**
 * @Author: deeprado
 * @Description  账户服务接口
 * @Date Created in 2019/9/5 16:37
 */
public interface AccountDubboService {

    /**
     * 从账户扣钱
     */
    ObjectResponse decreaseAccount(AccountDTO accountDTO);
}
