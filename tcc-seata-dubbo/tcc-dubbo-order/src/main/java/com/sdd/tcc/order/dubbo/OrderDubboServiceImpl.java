package com.sdd.tcc.order.dubbo;

import com.sdd.tcc.common.dto.OrderDTO;
import com.sdd.tcc.common.dubbo.OrderDubboService;
import com.sdd.tcc.common.response.ObjectResponse;
import com.sdd.tcc.order.service.ITOrderService;
import io.seata.core.context.RootContext;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: deeprado
 * @Description
 * @Date Created in 2019-09-04
 */
@DubboService(version = "1.0.0",protocol = "${dubbo.protocol.id}",
        application = "${dubbo.application.id}",registry = "${dubbo.registry.id}", group = "${dubbo.registry.group}",
        timeout = 3000)
@Slf4j
public class OrderDubboServiceImpl implements OrderDubboService {

    @Autowired
    private ITOrderService orderService;

    @Override
    public ObjectResponse<OrderDTO> createOrder(OrderDTO orderDTO) {
        log.info("全局事务id ：" + RootContext.getXID());
        return orderService.createOrder(orderDTO);
    }
}
