package com.sdd.tcc.order.service;

import com.baomidou.mybatisplus.service.IService;
import com.sdd.tcc.common.dto.OrderDTO;
import com.sdd.tcc.common.response.ObjectResponse;
import com.sdd.tcc.order.entity.TOrder;

/**
 * <p>
 *  创建订单
 * </p>
 *
 * * @author deeprado
 * @since 2019-09-04
 */
public interface ITOrderService extends IService<TOrder> {

    /**
     * 创建订单
     */
    ObjectResponse<OrderDTO> createOrder(OrderDTO orderDTO);
}
