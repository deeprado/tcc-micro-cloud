package com.sdd.tcc.business.controller;

import com.sdd.tcc.business.service.BusinessService;
import com.sdd.tcc.common.dto.BusinessDTO;
import com.sdd.tcc.common.response.ObjectResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

/**
 * @Author: deeprado
 * @Description Dubbo业务执行入口
 * @Date Created in 2019/1/14 17:15
 */
@RestController
@RequestMapping("/business/dubbo")
@Slf4j
public class BusinessController {


    @Autowired
    private BusinessService businessService;

    /**
     * 模拟用户购买商品下单业务逻辑流程
     *
     * @Param:
     * @Return:
     */
    @PostMapping("/buy")
    ObjectResponse handleBusiness(@RequestBody BusinessDTO businessDTO) {
        log.info("请求参数：{}", businessDTO.toString());
        return businessService.handleBusiness(businessDTO);
    }

    /**
     * 模拟用户购买商品下单业务异常逻辑流程
     *
     * @Param:
     * @Return:
     */
    @PostMapping("/buy2")
    ObjectResponse handleBusiness2(@RequestBody BusinessDTO businessDTO) {
        log.info("请求参数：{}", businessDTO.toString());
        return businessService.handleBusiness2(businessDTO);
    }

    @GetMapping("/test1")
    ObjectResponse test1() {
        BusinessDTO businessDTO = new BusinessDTO("1",
                "C201901140001",
                "水杯",
                1,
                new BigDecimal(20));
        log.info("请求参数：{}", businessDTO.toString());
        return businessService.handleBusiness(businessDTO);
    }


    @GetMapping("/test2")
    ObjectResponse test2() {
        BusinessDTO businessDTO = new BusinessDTO("1",
                "C201901140001",
                "水杯",
                1,
                new BigDecimal(5000));
        log.info("请求参数：{}", businessDTO.toString());
        return businessService.handleBusiness2(businessDTO);
    }
}
