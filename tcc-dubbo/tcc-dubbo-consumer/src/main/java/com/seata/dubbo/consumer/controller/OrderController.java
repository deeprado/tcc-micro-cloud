package com.seata.dubbo.consumer.controller;


import com.seata.dubbo.api.AccountApi;
import com.seata.dubbo.api.DubboApi;
import com.seata.dubbo.api.OrderApi;
import com.seata.dubbo.api.StorageApi;
import com.seata.dubbo.db.entity.Order;
import com.seata.dubbo.db.service.IOrderService;
import io.seata.spring.annotation.GlobalTransactional;
import org.apache.dubbo.config.annotation.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

/**
 * @author deeprado
 */
@RestController
@RequestMapping(value = "order")
public class OrderController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private LoadBalancerClient loadBalancerClient;

    private RestTemplate restTemplate = new RestTemplate();

    @Reference(check = false)
    private OrderApi orderApi;

    @Reference(check = false)
    private AccountApi accountApi;

    @Reference(check = false)
    private StorageApi storageApi;

    @GetMapping("test")
    public String test(){
        Order order = new Order();
        order.setCount(1);
        order.setMoney(new BigDecimal(2));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);


        orderApi.createOrder(order);
        //

        return "Create order success";
    }


    @GetMapping("test2")
    public String test2(){
        Order order = new Order();
        order.setCount(1);
        order.setMoney(new BigDecimal(200));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);
        orderApi.createOrder(order);
        return "Create order success";
    }


    @GetMapping("test3")
    public String test3(){
        Order order = new Order();
        order.setCount(15);
        order.setMoney(new BigDecimal(3));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);
        orderApi.createOrder(order);
        return "Create order success";
    }

    @GetMapping("test4")
    public String test4(){
        Order order = new Order();
        // 库存异常
        order.setCount(15);
        // 账户金额异常
        order.setMoney(new BigDecimal(200));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);
        orderApi.createOrder(order);
        return "Create order success";
    }


    @GetMapping("test5")
    @GlobalTransactional(name = "fsp_create_order2", timeoutMills = 60000)
    public String test5(){
        Order order = new Order();
        // 库存异常
        order.setCount(15);
        // 账户金额异常
        order.setMoney(new BigDecimal(200));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);

        //
        orderApi.create(order);

        //远程方法 扣减库存
        storageApi.decrease(order.getProductId(), order.getCount());

        //远程方法 扣减账户余额
        accountApi.decrease(order.getUserId(), order.getMoney());
        //

        return "Create order success";
    }


    /**
     * 创建订单
     * @param order
     * @return
     */
    @GetMapping("create")
    public String create(Order order){
        orderApi.createOrder(order);
        return "Create order success";
    }

    /**
     * 修改订单状态
     * @param userId
     * @param money
     * @param status
     * @return
     */
    @RequestMapping("update")
    String update(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money, @RequestParam("status") Integer status){
        orderApi.updateOrder(userId,money,status);
        return "订单状态修改成功";
    }

    @GetMapping("updateCount")
    String updateCount(@RequestParam("count") Integer count){
        orderApi.changeCount(count);
        return "更新成功";
    }
}
