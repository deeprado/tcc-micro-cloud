package com.seata.dubbo.provider;


//import com.alibaba.nacos.client.naming.cache.ServiceInfoHolder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = {"com.seata.dubbo.db", "com.seata.dubbo.provider"})
@EnableDiscoveryClient
@EnableFeignClients
public class DubboProviderApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboProviderApplication.class, args);
    }

}

