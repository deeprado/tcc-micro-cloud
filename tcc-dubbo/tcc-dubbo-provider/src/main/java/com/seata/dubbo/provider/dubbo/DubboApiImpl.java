package com.seata.dubbo.provider.dubbo;

import com.seata.dubbo.api.DubboApi;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 */
@DubboService
public class DubboApiImpl implements DubboApi {

    @Value("${server.port}")
    private Integer port;

    @Override
    public String demo() {
        return "port= "+ port + ", this is dubbo ";
    }

}
