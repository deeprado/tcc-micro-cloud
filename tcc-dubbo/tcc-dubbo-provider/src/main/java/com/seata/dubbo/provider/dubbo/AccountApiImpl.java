package com.seata.dubbo.provider.dubbo;

import com.seata.dubbo.api.AccountApi;
import com.seata.dubbo.api.DubboApi;
import com.seata.dubbo.db.entity.Account;
import com.seata.dubbo.db.service.IAccountService;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;

/**
 *
 */
@DubboService
public class AccountApiImpl implements AccountApi {

    @Value("${server.port}")
    private Integer port;

    @Autowired
    private IAccountService accountServiceImpl;


    @Override
    public String demo() {
        return "port= "+ port;
    }

    @Override
    public String decrease(Long userId, BigDecimal money) {
        accountServiceImpl.decrease(userId,money);
        return "Account decrease success";
    }
}
