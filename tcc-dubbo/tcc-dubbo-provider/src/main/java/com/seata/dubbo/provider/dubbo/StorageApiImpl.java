package com.seata.dubbo.provider.dubbo;

import com.seata.dubbo.api.StorageApi;
import com.seata.dubbo.db.service.IStorageService;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/**
 *
 */
@DubboService
public class StorageApiImpl implements StorageApi {

    @Value("${server.port}")
    private Integer port;

    @Autowired
    private IStorageService storageServiceImpl;


    @Override
    public String demo() {
        return "port= "+ port;
    }

    @Override
    public String decrease(Long productId, Integer count) {
        storageServiceImpl.decrease(productId,count);
        return "Decrease storage success";
    }
}
