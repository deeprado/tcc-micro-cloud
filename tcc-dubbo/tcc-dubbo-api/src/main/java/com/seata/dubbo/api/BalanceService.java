package com.seata.dubbo.api;

import com.seata.dubbo.api.pojo.Balance;

public interface BalanceService {

    Balance getBalance(Integer id);
}