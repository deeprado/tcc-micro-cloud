package com.seata.dubbo.api;


import com.seata.dubbo.db.entity.Order;

import java.math.BigDecimal;

/**
 *
 */
public interface OrderApi {

    String demo();

    String create(Order order);

    String createOrder(Order order);

    String updateOrder(Long userId, BigDecimal money, Integer status);

    String changeCount(Integer count);
}
