package com.seata.dubbo.api;


import java.math.BigDecimal;

/**
 *
 */
public interface AccountApi {

    String demo();

    String decrease(Long userId, BigDecimal money);
}
