package com.seata.dubbo.api;

import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 */
public interface StorageApi {

    String demo();

    String decrease( Long productId, Integer count);
}
