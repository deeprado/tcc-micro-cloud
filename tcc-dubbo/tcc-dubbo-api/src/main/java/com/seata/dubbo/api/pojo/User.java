package com.seata.dubbo.api.pojo;

public class User {

    private int id;
    private String name;
    private Balance balance;

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public User() {
    }

    //setter/getter略
}