package com.seata.dubbo.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seata.dubbo.db.entity.Product;
import com.seata.dubbo.db.mapper.ProductMapper;
import com.seata.dubbo.db.service.IProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

}
