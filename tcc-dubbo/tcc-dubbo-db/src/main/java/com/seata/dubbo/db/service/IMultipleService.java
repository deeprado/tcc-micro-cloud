package com.seata.dubbo.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.dubbo.db.entity.Orders;

public interface IMultipleService extends IService<Orders> {
}
