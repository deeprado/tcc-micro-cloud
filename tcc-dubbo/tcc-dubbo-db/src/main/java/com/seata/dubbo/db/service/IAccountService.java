package com.seata.dubbo.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.dubbo.db.entity.Account;

import java.math.BigDecimal;

/**
 *
 * @author deeprado
 */
public interface IAccountService extends IService<Account> {
    /**
     * 扣减账户余额
     * @param userId 用户id
     * @param money 金额
     */
    void decrease(Long userId, BigDecimal money);
}
