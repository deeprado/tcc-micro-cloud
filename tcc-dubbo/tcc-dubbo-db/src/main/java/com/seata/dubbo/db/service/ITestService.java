package com.seata.dubbo.db.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.dubbo.db.entity.Test;

/**
 * <p>
 * 功能 服务类
 * </p>
 *
 * @author deeprado
 * @since 2019-04-10
 */
public interface ITestService extends IService<Test> {

    Object Commit();
}