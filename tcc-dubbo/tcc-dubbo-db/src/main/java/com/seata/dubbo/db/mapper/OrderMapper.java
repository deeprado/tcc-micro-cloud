package com.seata.dubbo.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.dubbo.db.entity.Order;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;


/**
 *
 * @author deeprado
 * @since 2019-12-07
 */
public interface OrderMapper extends BaseMapper<Order> {
    /**
     * 创建订单
     * @param order
     * @return
     */
    void createOrder(Order order);

    /**
     * 修改订单金额
     * @param userId
     * @param money
     */
    void updateOrder(@Param("userId") Long userId, @Param("money") BigDecimal money,
                @Param("status") Integer status);


    void changeCount(Integer count);

}
