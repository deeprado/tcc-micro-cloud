package com.seata.dubbo.db.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.dubbo.db.entity.Test;

/**
 * <p>
 * 功能 Mapper 接口
 * </p>
 *
 * @author deeprado
 * @since 2019-04-10
 */
public interface TestMapper extends BaseMapper<Test> {

}