package com.seata.dubbo.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.dubbo.db.entity.Product;


/**
 *
 * @author deeprado
 * @since 2019-12-07
 */
public interface ProductMapper extends BaseMapper<Product> {

}
