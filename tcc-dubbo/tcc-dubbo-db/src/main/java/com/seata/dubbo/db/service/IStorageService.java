package com.seata.dubbo.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.dubbo.db.entity.Order;
import com.seata.dubbo.db.entity.Storage;

import java.math.BigDecimal;

/**
 * <p>
 * 功能 服务类
 * </p>
 *
 * @author deeprado
 * @since 2019-04-10
 */
public interface IStorageService extends IService<Storage> {

    /**
     * 扣减库存
     * @param productId 产品id
     * @param count 数量
     * @return
     */
    void decrease(Long productId, Integer count);

    String getById(Long productId);
}
