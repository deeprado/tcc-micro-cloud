package com.seata.dubbo.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.dubbo.db.entity.Account;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;

/**
 *
 * @author deeprado
 */
public interface AccountMapper extends BaseMapper<Account> {


    /**
     * 扣减账户余额
     * @param userId 用户id
     * @param money 金额
     */
    void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);
}
