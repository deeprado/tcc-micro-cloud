package com.seata.dubbo.db.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.seata.dubbo.db.entity.Order;
import com.seata.dubbo.db.entity.Storage;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;


/**
 *
 * @author deeprado
 * @since 2019-12-07
 */
public interface StorageMapper extends BaseMapper<Storage> {

    /**
     * 扣减库存
     * @param productId 产品id
     * @param count 数量
     * @return
     */
    void decrease(@Param("productId") Long productId, @Param("count") Integer count);
    void decrease2(@Param("productId") Long productId, @Param("count") Integer count);
    void decrease3(@Param("productId") Long productId, @Param("count") Integer count);
    void decrease4(@Param("productId") Long productId, @Param("count") Integer count);
    void decrease5(@Param("productId") Long productId, @Param("count") Integer count);

    Integer getById(Long productId);

}
