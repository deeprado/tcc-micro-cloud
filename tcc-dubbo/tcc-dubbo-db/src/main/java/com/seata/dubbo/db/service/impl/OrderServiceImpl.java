package com.seata.dubbo.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seata.dubbo.db.entity.Order;
import com.seata.dubbo.db.mapper.OrderMapper;
import com.seata.dubbo.db.service.IOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private OrderMapper orderDao;

    /**
     * 创建订单
     *
     * @param order
     * @return 测试结果：
     * 1.添加本地事务：仅仅扣减库存
     * 2.不添加本地事务：创建订单，扣减库存
     */
    @Override
//    @GlobalTransactional(name = "fsp_create_order",timeoutMills = 60000)
    public void createOrder(Order order) {
        LOGGER.info("------->order 交易开始");
        //本地方法
        orderDao.createOrder(order);

//        storageApi.getById(order.getProductId());

//        //远程方法 扣减库存
//        storageApi.decrease(order.getProductId(),order.getCount());
//
//        //远程方法 扣减账户余额
//        accountApi.decrease(order.getUserId(),order.getMoney());

        LOGGER.info("------->order 交易结束");
    }


    /**
     * 修改订单状态
     */
    @Override
    public void updateOrder(Long userId, BigDecimal money, Integer status) {
        LOGGER.info("修改订单状态，入参为：userId={},money={},status={}", userId, money, status);
        orderDao.updateOrder(userId, money, status);
    }

    /**
     * 修改订单数量
     * @param count
     */
    @Override
    public void changeCount(Integer count) {
        LOGGER.info("修改订单状态，入参为：count={}", count);
        orderDao.changeCount(count);
    }
}
