package com.seata.dubbo.db.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.seata.dubbo.db.entity.Order;

import java.math.BigDecimal;

/**
 * <p>
 * 功能 服务类
 * </p>
 *
 * @author deeprado
 * @since 2019-04-10
 */
public interface IOrderService extends IService<Order> {


    /**
     * 创建订单
     * @param order
     * @return
     */
    void createOrder(Order order);


    /**
     * 修改订单状态
     * @param userId
     * @param money
     * @param status
     */
    void updateOrder(Long userId, BigDecimal money, Integer status);

    void changeCount(Integer count);
}
