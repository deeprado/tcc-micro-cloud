package com.seata.dubbo.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seata.dubbo.db.entity.Account;
import com.seata.dubbo.db.mapper.AccountMapper;
import com.seata.dubbo.db.service.IAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);
    private final BigDecimal ERROR_MONEY = new BigDecimal("200");

    @Autowired
    private AccountMapper accountDao;

    /**
     * 扣减账户余额
     *
     * @param userId 用户id
     * @param money  金额
     */
    @Override
    public void decrease(Long userId, BigDecimal money) {
        LOGGER.info("------->扣减账户开始account中");
        //模拟超时异常，全局事务回滚 条件：money=200
        if (ERROR_MONEY.compareTo(money) == 0) {
            throw new RuntimeException("非法参数,money为：" + money);
        }
        accountDao.decrease(userId, money);
        LOGGER.info("------->扣减账户结束account中");
    }
}
