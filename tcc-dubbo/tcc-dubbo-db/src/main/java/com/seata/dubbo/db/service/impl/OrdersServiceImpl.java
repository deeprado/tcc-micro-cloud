package com.seata.dubbo.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.seata.dubbo.db.entity.Orders;
import com.seata.dubbo.db.mapper.OrdersMapper;
import com.seata.dubbo.db.service.IOrdersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements IOrdersService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

}
