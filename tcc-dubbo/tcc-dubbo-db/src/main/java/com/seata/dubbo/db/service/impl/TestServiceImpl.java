package com.seata.dubbo.db.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seata.dubbo.db.entity.Test;
import com.seata.dubbo.db.mapper.TestMapper;
import com.seata.dubbo.db.service.ITestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("ITestService")
public class TestServiceImpl extends ServiceImpl<TestMapper, Test> implements ITestService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Override
    @Transactional
    public Object Commit() {
        Test t = getOne(Wrappers.<Test>query().select("id", "two").eq("id", 1)
            .last("for update"));
        t.setTwo(t.getTwo() + 1);
        updateById(t);
        return true;
    }

}
