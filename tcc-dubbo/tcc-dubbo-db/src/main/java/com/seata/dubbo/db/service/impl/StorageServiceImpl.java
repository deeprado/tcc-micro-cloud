package com.seata.dubbo.db.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.seata.dubbo.db.entity.Order;
import com.seata.dubbo.db.entity.Storage;
import com.seata.dubbo.db.mapper.OrderMapper;
import com.seata.dubbo.db.mapper.StorageMapper;
import com.seata.dubbo.db.service.IOrderService;
import com.seata.dubbo.db.service.IStorageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
public class StorageServiceImpl extends ServiceImpl<StorageMapper, Storage> implements IStorageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StorageServiceImpl.class);

    @Autowired
    private StorageMapper storageDao;

    /**
     * 扣减库存
     * @param productId 产品id
     * @param count 数量
     * @return
     */
    @Override
    public void decrease(Long productId, Integer count) {
        LOGGER.info("------->扣减库存开始");
        //模拟服务超时
        if(count == 15){
            try {
                LOGGER.info("模拟超时"+ LocalDateTime.now().toString());
                Thread.sleep(45000);
                LOGGER.info("模拟超时"+ LocalDateTime.now().toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        storageDao.decrease(productId,count);
//        storageDao.decrease2(productId,count);
//        storageDao.decrease3(productId,count);
//        storageDao.decrease4(productId,count);
//        storageDao.decrease5(productId,count);
        LOGGER.info("------->扣减库存结束");
    }

    @Override
    public String getById(Long productId) {
        Integer used = storageDao.getById(productId);
        return used.toString();
    }
}
