package com.deep.seata.storage.mapper;

import com.deep.seata.common.model.storage.Storage;

/**
 * 仓储mapper
 * 
 * @author deeprado
 * @time 2019年6月12日
 */
public interface StorageMapper {

	/**
	 * 新增
	 * 
	 * @param storage
	 * @return
	 * @author deeprado
	 * @time 2019年6月12日
	 */
	int insert(Storage storage);

}
