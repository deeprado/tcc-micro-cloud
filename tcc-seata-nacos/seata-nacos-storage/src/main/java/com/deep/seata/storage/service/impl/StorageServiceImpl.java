package com.deep.seata.storage.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.deep.seata.common.model.storage.Storage;
import com.deep.seata.storage.mapper.StorageMapper;
import com.deep.seata.storage.service.StorageService;

import io.seata.core.context.RootContext;

/**
 * 仓储service实现
 *
 * @author deeprado
 * @time 2019年6月12日
 */
@RestController
public class StorageServiceImpl implements StorageService {
    private final static Logger logger = LoggerFactory.getLogger(StorageServiceImpl.class);

    @Autowired
    private StorageMapper storageMapper;

    /**
     * 新增
     *
     * @param storage
     * @return
     * @author deeprado
     * @time 2019年6月12日
     */
    @Override
    public Map<String, Object> insert(@RequestBody Storage storage) {
        logger.info("RootContext.getXID {}", RootContext.getXID());

        logger.info("Storage {}", storage);


        storageMapper.insert(storage);
//		int a = 10/0;
        if (storage.getStorageCount().equals(10)) {
            int a = 10 / 0;
        }

        Map<String, Object> result = new HashMap<>(16);
        result.put("status", 200);
        result.put("message", "新增成功！");
        return result;
    }
}
