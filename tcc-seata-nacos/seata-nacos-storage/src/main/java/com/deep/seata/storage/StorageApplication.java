package com.deep.seata.storage;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * storage 启动类
 * 
 * @author deeprado
 * @time 2019年6月12日
 */
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.deep.seata.storage.mapper")
public class StorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(StorageApplication.class, args);
	}

}
