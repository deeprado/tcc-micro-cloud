package com.deep.seata.account;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * account 启动类
 * 
 * @author deeprado
 * @time 2019年6月12日
 */
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.deep.seata.account.mapper")
public class AccountApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountApplication.class, args);
	}

}
