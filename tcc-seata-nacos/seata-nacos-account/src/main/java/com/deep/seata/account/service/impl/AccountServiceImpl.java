package com.deep.seata.account.service.impl;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.deep.seata.account.mapper.AccountMapper;
import com.deep.seata.account.service.AccountService;
import com.deep.seata.common.model.account.Account;

import io.seata.core.context.RootContext;

/**
 * 账户service实现
 * 
 * @author deeprado
 * @time 2019年6月12日
 */
@RestController
public class AccountServiceImpl implements AccountService {
	private static Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);

	@Autowired
	private AccountMapper accountMapper;

	/**
	 * 新增
	 * 
	 * @param account
	 * @return
	 * @author deeprado
	 * @time 2019年6月12日
	 */
	@Override
	public Map<String, Object> insert(Account account) {
		log.info("AccountServiceImpl insert xid {} account = {}", RootContext.getXID(), account);


		accountMapper.insert(account);
		//int a = 10/0;

		if (account.getAmount().equals(new BigDecimal(10))) {
			int a = 10/0;
		}

		Map<String, Object> result = new HashMap<>(16);
		result.put("status", 200);
		result.put("message", "新增成功！");
		return result;
	}

}
