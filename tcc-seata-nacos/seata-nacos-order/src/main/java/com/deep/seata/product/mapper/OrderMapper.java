package com.deep.seata.product.mapper;

import com.deep.seata.common.model.order.Order;

/**
 * 订单mapper
 * 
 * @author deeprado
 * @time 2019年6月12日
 */
public interface OrderMapper {

	/**
	 * 新增
	 * 
	 * @param order
	 * @return
	 * @author deeprado
	 * @time 2019年6月12日
	 */
	int insert(Order order);

}
