package com.deep.seata.product.service;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.deep.seata.common.model.order.Order;

/**
 * Order feign客户端
 * 
 * @author deeprado
 * @time 2019年6月12日
 */
//@FeignClient(name = "seata-nacos-order", fallback = OrderServiceHystrixImpl.class)
@FeignClient(name = "seata-nacos-order")
public interface OrderService {

	/**
	 * 新增
	 * 
	 * @param order
	 * @return
	 * @author deeprado
	 * @time 2019年6月12日
	 */
	@RequestMapping(value = "/order/insert", method = RequestMethod.POST)
	Map<String, Object> insert(@RequestBody Order order);

}
