package com.deep.seata.business;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * business 启动类
 * @author deeprado
 * @time 2019年6月12日
 */
@EnableDiscoveryClient
@EnableFeignClients(basePackages="com.deep.seata")
@EnableCircuitBreaker
@SpringBootApplication
@ComponentScan(basePackages="com.deep.seata")
public class BusinessApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusinessApplication.class, args);
	}

}
