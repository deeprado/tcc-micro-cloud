package com.deep.seata.storage.service;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.deep.seata.common.model.storage.Storage;
import com.deep.seata.storage.service.hystrix.StorageServiceHystrixImpl;

/**
 * Storage feign客户端
 * 
 * @author deeprado
 * @time 2019年6月12日
 */
//@FeignClient(name = "seata-nacos-storage", fallback = StorageServiceHystrixImpl.class)
@FeignClient(name = "seata-nacos-storage")
public interface StorageService {

	/**
	 * 新增
	 * 
	 * @param storage
	 * @return
	 * @author deeprado
	 * @time 2019年6月12日
	 */
//	@RequestMapping(value = "/storage/insert", method = RequestMethod.POST)
	@PostMapping(value = "/storage/insert")
	public Map<String, Object> insert(@RequestBody Storage storage);

}
