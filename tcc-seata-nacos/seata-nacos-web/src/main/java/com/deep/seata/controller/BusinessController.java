package com.deep.seata.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.deep.seata.business.service.BusinessService;

/**
 * 业务controller
 *
 * @author deeprado
 * @time 2019年6月12日
 */
@Controller
@RequestMapping("/business")
public class BusinessController {
    private static Logger logger = LoggerFactory.getLogger(BusinessController.class);

    @Autowired
    private BusinessService businessService;

    /**
     * 购买
     *
     * @param request
     * @param response
     * @return
     * @author deeprado
     * @time 2019年6月12日
     */
    @ResponseBody
    @RequestMapping("/purchase")
    public Object purchase(HttpServletRequest request, HttpServletResponse response) {
        String accountId = "1";
        String orderId = "2";
        String storageId = "3";
        String debug = "";

        logger.info("accountId {}, orderId {}, storageId {}", accountId, orderId, storageId);

        Map<String, Object> result = businessService.purchase(accountId, orderId, storageId, debug);
        return result;
    }

    /**
     * accountId
     *
     * @param accountId
     * @param orderId
     * @param storageId
     * @return
     */
    @ResponseBody
    @RequestMapping("/purchase2")
    public Object purchase2(@RequestParam(defaultValue = "1") String accountId,
                            @RequestParam(defaultValue = "2") String orderId,
                            @RequestParam(defaultValue = "3") String storageId,
                            @RequestParam(name = "debug", defaultValue = "account") String debug) {

        logger.info("accountId {}, orderId {}, storageId {} debug {}", accountId, orderId, storageId, debug);

        Map<String, Object> result = businessService.purchase(accountId, orderId, storageId, debug);
        return result;
    }
}
