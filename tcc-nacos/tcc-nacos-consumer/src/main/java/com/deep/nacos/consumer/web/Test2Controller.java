package com.deep.nacos.consumer.web;

import com.deep.nacos.consumer.client.EchoFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Test2Controller {

    @Autowired
    private EchoFeign echoFeign;

    @RequestMapping(value = "/v2/echo/{str}", method = RequestMethod.GET)
    public String echo(@PathVariable String str) {
        return echoFeign.getEcho(str);
    }

    @RequestMapping(value = "/v2/config/{str}", method = RequestMethod.GET)
    public String config(@PathVariable String str) {
        return echoFeign.getConfig(str);
    }
}