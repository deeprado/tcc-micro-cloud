package com.deep.nacos.zipkin.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

//@FeignClient(value = "tcc-nacos-track", fallbackFactory = RouterFeignBackFactory.class, path = "/echo")
@FeignClient(value = "tcc-nacos-track", fallbackFactory = RouterFeignBackFactory.class)
public interface EchoFeign {
    //
    @GetMapping("/echo/{string}")
    String getEcho(@RequestParam String string);

    //
    @GetMapping("/config/get/{string}")
    String getConfig(@RequestParam String string);


    //
    @GetMapping("/echo2/{string}")
    String getEcho2(@RequestParam String string);

    //
    @GetMapping("/echo3/{string}")
    String getEcho3(@RequestParam String string);

    //
    @GetMapping("/hello/{string}")
    String getHello(@RequestParam String string);

}
