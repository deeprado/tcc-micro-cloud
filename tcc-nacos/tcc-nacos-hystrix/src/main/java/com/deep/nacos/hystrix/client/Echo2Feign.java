package com.deep.nacos.hystrix.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 如果我们要创建多个具有相同名称或URL的伪客户端，以便它们指向同一台服务器，但每个客户端具有不同的自定义配置，则必须使用的contextId属性
 */
//@FeignClient(value = "tcc-nacos-provider", fallbackFactory = RouterFeign2BackFactory.class, path = "/echo")
@FeignClient(value = "tcc-nacos-provider", contextId = "home", fallbackFactory = RouterFeign2BackFactory.class)
public interface Echo2Feign {
    //
    @GetMapping("/echo/{string}")
    String getEcho(@RequestParam String string);

    //
    @GetMapping("/config/get/{string}")
    String getConfig(@RequestParam String string);


    //
    @GetMapping("/echo2/{string}")
    String getEcho2(@RequestParam String string);

    //
    @GetMapping("/echo3/{string}")
    String getEcho3(@RequestParam String string);

}
