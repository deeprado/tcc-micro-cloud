package com.deep.nacos.hystrix.web;

import com.deep.nacos.hystrix.client.EchoFeign;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Hystrix3Controller {

    @Autowired
    private EchoFeign echoFeign;

    /**
     * 服务调用
     *
     * @param str
     * @return
     */
    @HystrixCommand(fallbackMethod = "getNameFallback")
    @RequestMapping(value = "/v3/echo/{str}", method = RequestMethod.GET)
    public String echo(@PathVariable String str) {
        return echoFeign.getEcho(str);
    }

    /**
     * TODO 超时处理
     *  使用oepnfeign 后，配置无效
     *  需要启用 feign-hystrix 并配置ribbon 等连接、读取时间
     * @param str
     * @return
     */
    @HystrixCommand(fallbackMethod = "getNameFallback2", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "120000")
    })
    @RequestMapping(value = "/v3/echo2/{str}", method = RequestMethod.GET)
    public String echo2(@PathVariable String str) {
        return echoFeign.getEcho2(str);
    }

    /**
     * 服务异常
     *
     * @param str
     * @return
     */
    @HystrixCommand(fallbackMethod = "getNameFallback3",
            threadPoolKey = "hystrixDemoThreadPool3",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "30"),
                    @HystrixProperty(name = "maxQueueSize", value = "10")
            })
    @RequestMapping(value = "/v3/echo3/{str}", method = RequestMethod.GET)
    public String echo3(@PathVariable String str) {
        return echoFeign.getEcho3(str);
    }




    /**
     * 出错后会调用该降级方法，返回指定的信息
     *
     * @param username
     * @return
     */
    public String getNameFallback(String username) {
        return " this username is not exist v3";
    }

    /**
     * 服务超时
     *
     * @param username
     * @return
     */
    public String getNameFallback2(String username) {
        return " provider timeout v3 ";
    }

    /**
     * 服务异常
     *
     * @param username
     * @return
     */
    public String getNameFallback3(String username) {
        return " provider exception v3";
    }
}