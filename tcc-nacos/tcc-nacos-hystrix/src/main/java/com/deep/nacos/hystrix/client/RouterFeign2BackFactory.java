package com.deep.nacos.hystrix.client;


import feign.hystrix.FallbackFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * openfeign - hystrix , 和单独 openfeign 不同
 */
@Slf4j
@Component
public class RouterFeign2BackFactory implements FallbackFactory<Echo2Feign> {

    @Override
    public Echo2Feign create(Throwable throwable) {
        log.error("请求出错:" + throwable.getMessage());
        return new Echo2Feign() {

            @Override
            public String getEcho(String string) {
                return " getEcho fallback";
            }

            @Override
            public String getConfig(String string) {
                return " getConfig fallback";
            }

            @Override
            public String getEcho2(String string) {
                return " getEcho2 fallback";
            }

            @Override
            public String getEcho3(String string) {
                return " getEcho3 fallback";
            }
        };
    }

}
