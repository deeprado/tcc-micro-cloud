package com.deep.nacos.hystrix.web;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Hystrix2Controller {

    private final RestTemplate restTemplate;

    @Autowired
    public Hystrix2Controller(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * 服务调用
     *
     * @param str
     * @return
     */
    @HystrixCommand(fallbackMethod = "getNameFallback")
    @RequestMapping(value = "/v2/echo/{str}", method = RequestMethod.GET)
    public String echo(@PathVariable String str) {
        return restTemplate.getForObject("http://tcc-nacos-provider/echo/" + str, String.class);
    }

    /**
     * 超时处理
     *
     * @param str
     * @return
     */
    @HystrixCommand(fallbackMethod = "getNameFallback2", commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "120000")
    })
    @RequestMapping(value = "/v2/echo2/{str}", method = RequestMethod.GET)
    public String echo2(@PathVariable String str) {
        return restTemplate.getForObject("http://tcc-nacos-provider/echo2/" + str, String.class);
    }

    /**
     * 服务异常
     *
     * @param str
     * @return
     */
    @HystrixCommand(fallbackMethod = "getNameFallback3",
            threadPoolKey = "hystrixDemoThreadPool2",
            threadPoolProperties = {
                    @HystrixProperty(name = "coreSize", value = "30"),
                    @HystrixProperty(name = "maxQueueSize", value = "10")
            })
    @RequestMapping(value = "/v2/echo3/{str}", method = RequestMethod.GET)
    public String echo3(@PathVariable String str) {
        return restTemplate.getForObject("http://tcc-nacos-provider/echo3/" + str, String.class);
    }


    /**
     * 出错后会调用该降级方法，返回指定的信息
     *
     * @param username
     * @return
     */
    public String getNameFallback(String username) {
        return " this username is not exist v2 ";
    }

    /**
     * 服务超时
     *
     * @param username
     * @return
     */
    public String getNameFallback2(String username) {
        return " provider timeout v2";
    }

    /**
     * 服务异常
     *
     * @param username
     * @return
     */
    public String getNameFallback3(String username) {
        return " provider exception v2";
    }
}