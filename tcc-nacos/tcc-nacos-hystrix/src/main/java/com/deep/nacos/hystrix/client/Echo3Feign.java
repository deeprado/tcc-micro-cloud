package com.deep.nacos.hystrix.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * name 和 value 只能写一个， 异常： Different @AliasFor mirror values for annotation
 */
//@FeignClient(name="FeignClient3", value = "tcc-nacos-provider", fallbackFactory = RouterFeign3BackFactory.class, path = "/echo")
//@FeignClient(value = "tcc-nacos-provider", fallbackFactory = RouterFeign3BackFactory.class)
public interface Echo3Feign {
    //
    @GetMapping("/echo/{string}")
    String getEcho(@RequestParam String string);

    //
    @GetMapping("/config/get/{string}")
    String getConfig(@RequestParam String string);


    //
    @GetMapping("/echo2/{string}")
    String getEcho2(@RequestParam String string);

    //
    @GetMapping("/echo3/{string}")
    String getEcho3(@RequestParam String string);

}
