package com.deep.nacos.sentinel.service;

import org.springframework.stereotype.Component;

@Component
public class HelloServiceSentinelFallback implements HelloService {


    /**
     * 调用服务提供方的输出接口.
     *
     * @param str 用户输入
     * @return
     */
    @Override
    public String hello(String str) {
        return "服务调用失败，降级处理。异常信息：";
    }
}


