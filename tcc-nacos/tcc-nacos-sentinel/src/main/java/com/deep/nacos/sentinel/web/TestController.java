package com.deep.nacos.sentinel.web;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.deep.nacos.sentinel.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@ResponseBody
public class TestController {

    @Autowired
    private HelloService helloService;


    @GetMapping("hello")
    @SentinelResource(value = "test.hello", fallback = "helloError")
    public String hello(String name) {
        return "hello," + name;
    }

    public String helloError(String name, Throwable e) {
        return "error," + name;
    }

    /**
     * feign 自己的處理
     * @param str
     * @return
     */
    @GetMapping("/hello-feign/{str}")
    @SentinelResource(value = "test2.hello", fallback = "hello2Error")
    public String feign(@PathVariable String str) {
        return helloService.hello(str);
    }

    public String hello2Error(String name, Throwable e) {
        return "error," + name;
    }

    @GetMapping("/hello-sentinel/{str}")
    @SentinelResource(value = "test3.hello", fallback = "hello3Error")
    public String feign3(@PathVariable String str) {
        return helloService.hello(str);
    }

    public String hello3Error(String name, Throwable e) {
        return "error 3 ," + name;
    }

}