package com.deep.nacos.sentinel.service;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * provider 剛關閉時，是 connect timeout
 * 服務銷毀後，就是 com.netflix.client.ClientException: Load balancer does not have available server for client: tcc-nacos-provider
 */
//@FeignClient(name = "tcc-nacos-provider", fallbackFactory = HelloServiceFallbackFactory.class)
@FeignClient(name = "tcc-nacos-provider", fallback = HelloServiceSentinelFallback.class)
public interface HelloService {

    /**
     * 调用服务提供方的输出接口.
     *
     * @param str 用户输入
     * @return hello result
     */
    @GetMapping("/hello/{str}")
    String hello(@PathVariable("str") String str);
}

