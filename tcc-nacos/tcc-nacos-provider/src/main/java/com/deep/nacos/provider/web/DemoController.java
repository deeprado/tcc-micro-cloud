package com.deep.nacos.provider.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {


    @GetMapping("/")
    public String home() {
        return "this is nacos docker";
    }

    @GetMapping("/health")
    public String health() {
        return "this is nacos health";
    }

    @GetMapping("/timer")
    public String timer() {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "this is nacos timer";
    }

}
