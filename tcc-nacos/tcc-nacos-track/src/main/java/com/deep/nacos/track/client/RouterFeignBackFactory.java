package com.deep.nacos.track.client;

public class RouterFeignBackFactory implements EchoFeign {
    @Override
    public String getEcho(String string) {
        return " getEcho fallback";
    }

    @Override
    public String getConfig(String string) {
        return " getConfig fallback";
    }

    @Override
    public String getEcho2(String string) {
        return " getEcho2 fallback";
    }

    @Override
    public String getEcho3(String string) {
        return " getEcho3 fallback";
    }

    @Override
    public String getHello(String string) {
        return null;
    }
}
