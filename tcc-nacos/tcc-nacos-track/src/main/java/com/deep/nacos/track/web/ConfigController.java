package com.deep.nacos.track.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {

    @Autowired
    ConfigurableApplicationContext configurableApplicationContext;

    @Value("${useLocalCache:false}")
    private boolean useLocalCache;

    @Value("${name:temp}")
    private String name;


    @RequestMapping("/get")
    public boolean get() {
        return useLocalCache;
    }

    @RequestMapping("")
    public boolean test() {
        return useLocalCache;
    }

    @GetMapping("/hello")
    public String hello() {
        return name;
    }

    @RequestMapping(value = "/get/{keyName}", method = RequestMethod.GET)
    public String echo(@PathVariable String keyName) {
        System.out.println(keyName);
        String value = configurableApplicationContext.getEnvironment().getProperty(keyName);
        return "Hello Nacos Discovery " + keyName + ", value = " + value;
    }
}