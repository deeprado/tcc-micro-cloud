package com.deep.nacos.track.web;

import com.deep.nacos.track.client.EchoFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Test2Controller {

    @Autowired
    private EchoFeign echoFeign;

    @RequestMapping(value = "/hello/{str}", method = RequestMethod.GET)
    public String hello(@PathVariable String str) {
        return echoFeign.getHello(str);
    }

    @RequestMapping(value = "/v2/echo/{str}", method = RequestMethod.GET)
    public String echo(@PathVariable String str) {
        return echoFeign.getEcho2(str);
    }

    @RequestMapping(value = "/v2/config/{str}", method = RequestMethod.GET)
    public String config(@PathVariable String str) {
        return echoFeign.getConfig(str);
    }
}