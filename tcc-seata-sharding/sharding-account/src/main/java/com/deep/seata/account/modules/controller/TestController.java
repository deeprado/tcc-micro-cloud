package com.deep.seata.account.modules.controller;

import com.deep.seata.account.modules.entity.OrderEntity;
import com.deep.seata.account.modules.service.IOrderService;
import com.deep.seata.common.dto.OrderEntityDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dell
 * @create 2022/7/1 8:23
 */
@RequestMapping("/test")
@RestController
@Slf4j
public class TestController {

    @Autowired
    private IOrderService orderService;

    @GetMapping("/order")
    public String order() {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setOrderId(123);
        orderEntity.setStatus("seata");
        orderEntity.setUserId(123);
        log.info("orderEntity", orderEntity);

        orderService.insertOrder(orderEntity);

        return "done";
    }
}
