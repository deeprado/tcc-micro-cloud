package com.deep.seata.account.modules.dubbo;

import com.deep.seata.account.modules.entity.OrderEntity;
import com.deep.seata.account.modules.service.IOrderService;
import com.deep.seata.common.dto.OrderEntityDto;
import com.deep.seata.common.dubbo.PurchaseService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.apache.dubbo.config.annotation.Service;
import org.apache.shardingsphere.transaction.core.TransactionType;
import org.apache.shardingsphere.transaction.core.TransactionTypeHolder;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author dell
 * @create 2022/6/30 22:41
 */
//@Service(version = "1.0.0",protocol = "${dubbo.protocol.id}",
//        application = "${dubbo.application.id}",registry = "${dubbo.registry.id}",
//        timeout = 3000)
//@Service(version = "1.0.0")
//@Service
@DubboService(version = "1.0.0", protocol = "${dubbo.protocol.id}",
        application = "${dubbo.application.id}", registry = "${dubbo.registry.id}", group = "${dubbo.registry.group}",
        timeout = 3000)
//@DubboService
@Slf4j
public class PurchaseServiceImpl implements PurchaseService {

    @Autowired
    private IOrderService orderService;

    @Override
    public String pay(OrderEntityDto orderEntityDto) {

        log.info("orderEntityDto = {}", orderEntityDto);
        OrderEntity orderEntity = new OrderEntity();
        BeanUtils.copyProperties(orderEntityDto, orderEntity);
        log.info("orderEntity = {}", orderEntity);

        orderService.insertOrder(orderEntity);

        return orderEntity.getId();
    }
}
