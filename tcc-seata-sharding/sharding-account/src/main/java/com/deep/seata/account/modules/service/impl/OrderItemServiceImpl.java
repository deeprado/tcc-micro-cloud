/*
 *  Copyright 1999-2021 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.deep.seata.account.modules.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.deep.seata.account.modules.entity.OrderItemEntity;
import com.deep.seata.account.modules.mapper.OrderItemMapper;
import com.deep.seata.account.modules.service.IOrderItemService;
import org.springframework.stereotype.Service;


@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItemEntity> implements IOrderItemService {

    @Override
    public void insertOrderItem(OrderItemEntity orderItemEntity) {
        baseMapper.insert(orderItemEntity);
    }
}
