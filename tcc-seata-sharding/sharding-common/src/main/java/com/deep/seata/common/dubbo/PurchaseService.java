package com.deep.seata.common.dubbo;

import com.deep.seata.common.dto.OrderEntityDto;

/**
 * @author dell
 * @create 2022/6/30 22:37
 */
public interface PurchaseService {
    String pay(OrderEntityDto orderEntityDto);
}
