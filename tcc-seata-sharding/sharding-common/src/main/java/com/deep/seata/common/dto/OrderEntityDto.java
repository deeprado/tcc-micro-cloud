package com.deep.seata.common.dto;

import java.io.Serializable;

/**
 * @author dell
 * @create 2022/6/30 22:38
 */
public class OrderEntityDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private long orderId;

    private int userId;

    private String status;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "OrderEntityDto{" +
                "orderId=" + orderId +
                ", userId=" + userId +
                ", status='" + status + '\'' +
                '}';
    }
}
