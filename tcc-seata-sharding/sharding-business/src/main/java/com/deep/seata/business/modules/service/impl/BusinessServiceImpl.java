/*
 *  Copyright 1999-2021 Seata.io Group.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.deep.seata.business.modules.service.impl;

import com.alibaba.dubbo.config.annotation.Reference;
import com.deep.seata.business.modules.service.IBusinessService;
import com.deep.seata.common.dto.OrderEntityDto;
import com.deep.seata.common.dubbo.PurchaseService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.shardingsphere.transaction.annotation.ShardingTransactionType;
import org.apache.shardingsphere.transaction.core.TransactionType;
import org.apache.shardingsphere.transaction.core.TransactionTypeHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class BusinessServiceImpl implements IBusinessService {

//    @Reference(version = "1.0.0", check = false)
//    @Reference(check = false)
//    @Reference
//    @DubboReference
    @DubboReference(version = "1.0.0", group = "${dubbo.registry.group}")
    private PurchaseService purchaseService;

    @Override
    @GlobalTransactional(name = "dubbo-purchase")
    public void purchase() {
        TransactionTypeHolder.set(TransactionType.BASE);
        OrderEntityDto orderEntity = new OrderEntityDto();
        orderEntity.setOrderId(123);
        orderEntity.setStatus("seata");
        orderEntity.setUserId(123);
        log.info("orderEntity = {}", orderEntity);

        String orderStrId = purchaseService.pay(orderEntity);
        log.info("orderStrId = {}", orderStrId);

        System.out.println("XID:" + RootContext.getXID());
//        throw new RuntimeException("回滚测试");
    }

    @Override
    @GlobalTransactional(name = "dubbo-purchase")
    public void purchase2() {
        TransactionTypeHolder.set(TransactionType.BASE);
        OrderEntityDto orderEntity = new OrderEntityDto();
        orderEntity.setOrderId(123);
        orderEntity.setStatus("seata");
        orderEntity.setUserId(123);
        log.info("orderEntity = {}", orderEntity);

        String orderStrId = purchaseService.pay(orderEntity);
        log.info("orderStrId = {}", orderStrId);

        System.out.println("XID:" + RootContext.getXID());
        throw new RuntimeException("回滚测试");
    }


    @Override
//    @GlobalTransactional(name = "dubbo-purchase")
    @Transactional(rollbackFor = Exception.class)
    @ShardingTransactionType(TransactionType.BASE)
    public void purchase3() {
//        TransactionTypeHolder.set(TransactionType.BASE);
        OrderEntityDto orderEntity = new OrderEntityDto();
        orderEntity.setOrderId(123);
        orderEntity.setStatus("seata");
        orderEntity.setUserId(123);
        log.info("orderEntity = {}", orderEntity);

        String orderStrId = purchaseService.pay(orderEntity);
        log.info("orderStrId = {}", orderStrId);

        log.info("XID:" + RootContext.getXID());
//        throw new RuntimeException("回滚测试");
    }

    @Override
//    @GlobalTransactional(name = "dubbo-purchase")
    @Transactional(rollbackFor = Exception.class)
    @ShardingTransactionType(TransactionType.BASE)
    public void purchase4() {
//        TransactionTypeHolder.set(TransactionType.BASE);
        OrderEntityDto orderEntity = new OrderEntityDto();
        orderEntity.setOrderId(123);
        orderEntity.setStatus("seata");
        orderEntity.setUserId(123);
        log.info("orderEntity = {}", orderEntity);

        String orderStrId = purchaseService.pay(orderEntity);
        log.info("orderStrId = {}", orderStrId);

        log.info("XID:" + RootContext.getXID());
        throw new RuntimeException("回滚测试");
    }


    ///////// ----------------- ---------------------- /////////////////

    @GlobalTransactional(name = "dubbo-purchase")
    public void error1() {
        TransactionTypeHolder.set(TransactionType.BASE);
        OrderEntityDto orderEntity = new OrderEntityDto();
        orderEntity.setOrderId(123);
        orderEntity.setStatus("seata");
        orderEntity.setUserId(123);
        log.info("orderEntity = {}", orderEntity);

        purchaseService.pay(orderEntity);

        log.info("XID:" + RootContext.getXID());
//        throw new RuntimeException("回滚测试");
    }

    @Transactional(rollbackFor = ArithmeticException.class)
    @ShardingTransactionType(TransactionType.BASE)
    public void error2() {
        OrderEntityDto orderEntity = new OrderEntityDto();
        orderEntity.setOrderId(123);
        orderEntity.setStatus("seata");
        orderEntity.setUserId(123);
        log.info("orderEntity = {}", orderEntity);

        purchaseService.pay(orderEntity);

        log.info("XID:" + RootContext.getXID());
//        throw new RuntimeException("回滚测试");
    }

    @Transactional(rollbackFor = Exception.class)
    @ShardingTransactionType(TransactionType.BASE)
    public void error3() {
        OrderEntityDto orderEntity = new OrderEntityDto();
        orderEntity.setOrderId(123);
        orderEntity.setStatus("seata");
        orderEntity.setUserId(123);
        log.info("orderEntity = {}", orderEntity);

        purchaseService.pay(orderEntity);

        log.info("XID:" + RootContext.getXID());
//        throw new RuntimeException("回滚测试");
    }
}
