package com.deep.seata.business.modules.aspect;

import org.apache.shardingsphere.transaction.core.TransactionTypeHolder;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * @author dell
 * @create 2022/7/1 8:46
 */
//@Component
@Aspect
@Order(-1)
public class ShardingTransactionAspect {
    private static Logger LOGGER = LoggerFactory.getLogger(ShardingTransactionAspect.class);


    @Pointcut("@within(此处填注解类路径.ShardingTransaction)|| @annotation(此处填注解类路径.ShardingTransaction)")
    public void pointCut() {

    }

    @Before("pointCut()")
    public void doBefore(JoinPoint point) {
//        ShardingTransaction shardingTransaction = point.getTarget().getClass().getAnnotation(ShardingTransaction.class);
//        if (shardingTransaction == null) {
//            //注解在类上
//            MethodSignature signature = (MethodSignature) point.getSignature();
//            Method method = signature.getMethod();
//            shardingTransaction = method.getAnnotation(ShardingTransaction.class);
//        }
//        TransactionTypeHolder.set(shardingTransaction.value());
//        LOGGER.info("选择Sharding分布式事务---" + shardingTransaction.value().toString());
    }


    @After("pointCut()")
    public void doAfter() {
        TransactionTypeHolder.clear();
    }
}
