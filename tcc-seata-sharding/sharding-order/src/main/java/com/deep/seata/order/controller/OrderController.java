package com.deep.seata.order.controller;

import com.deep.seata.order.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping("/seata/test")
    public ResponseEntity<Void> seataDemo(Boolean hasError) {
        orderService.seataDemo(hasError);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/seata/test2")
    public ResponseEntity<Void> seataDemo2(@RequestParam Boolean hasError) {
//        Boolean hasError
        orderService.seataDemo(hasError);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @GetMapping("/seata/test3")
    public ResponseEntity<Void> seataDemo3() {
//        Boolean hasError
        orderService.seataDemo2(false);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
