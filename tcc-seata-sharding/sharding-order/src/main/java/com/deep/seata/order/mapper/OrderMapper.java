package com.deep.seata.order.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.deep.seata.order.entity.Order;

public interface OrderMapper extends BaseMapper<Order> {}
