package com.deep.seata.order.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PutMapping;

@FeignClient("sharding-product")
public interface ProductClient {

    @PutMapping("/minus/stock")
    public Void minusStock();


    @PutMapping("/minus/stockError")
    public Void minusErrorStock();

}
