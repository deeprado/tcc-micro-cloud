package com.deep.seata.account.mapper;

import com.deep.seata.common.model.account.Account;

/**
 * 账户mapper
 * 
 * @author deeprado
 * @time 2019年6月12日
 */
public interface AccountMapper {

	/**
	 * 新增
	 * 
	 * @param account
	 * @return
	 * @author deeprado
	 * @time 2019年6月12日
	 */
	int insert(Account account);

}
