

1. process connectionProxy commit error: Unknown column 'context' in 'field list'

可能是seata中的undo_log表建表语句有问题导致的，将原先的表删掉，用这个重新建试下
-- 注意此处0.3.0+ 增加唯一索引 ux_undo_log
CREATE TABLE undo_log (
id bigint(20) NOT NULL AUTO_INCREMENT,
branch_id bigint(20) NOT NULL,
xid varchar(100) NOT NULL,
context varchar(128) NOT NULL,
rollback_info longblob NOT NULL,
log_status int(11) NOT NULL,
log_created datetime NOT NULL,
log_modified datetime NOT NULL,
ext varchar(100) DEFAULT NULL,
PRIMARY KEY (id),
UNIQUE KEY ux_undo_log (xid,branch_id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

1.3 和 1.4 版本有区别

2. 