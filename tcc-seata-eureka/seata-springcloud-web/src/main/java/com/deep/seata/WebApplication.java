package com.deep.seata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * web 启动类
 *
 * @author deeprado
 * @time 2019年6月12日
 */
@EnableEurekaClient
@EnableFeignClients(basePackages = "com.deep.seata")
@EnableCircuitBreaker
@SpringBootApplication(scanBasePackages = "com.deep.seata")
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

}
