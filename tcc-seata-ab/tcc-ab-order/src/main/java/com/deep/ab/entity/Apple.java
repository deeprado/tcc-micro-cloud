package com.deep.ab.entity;

import lombok.Data;

/**
 * @author wangzhongxiang
 */
@Data
public class Apple {

    private Integer id;

    private Integer count;

    private String name;
}
