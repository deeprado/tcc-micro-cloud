package com.deep.ab.controller;

import com.deep.ab.entity.Order;
import com.deep.ab.service.OrderService;
import com.deep.ab.service.OrderServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 * @author deeprado
 */
@RestController
@RequestMapping(value = "order")
public class OrderController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderServiceImpl;

    @GetMapping("test")
    public String test() {
        Order order = new Order();
        order.setCount(1);
        order.setMoney(new BigDecimal(2));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);
        orderServiceImpl.create(order);
        return "Create order success";
    }


    @GetMapping("test2")
    public String test2() {
        Order order = new Order();
        order.setCount(1);
        order.setMoney(new BigDecimal(200));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);
        orderServiceImpl.create(order);
        return "Create order success";
    }


    @GetMapping("test3")
    public String test3() {
        Order order = new Order();
        order.setCount(15);
        order.setMoney(new BigDecimal(3));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);
        orderServiceImpl.create(order);
        return "Create order success";
    }

    @GetMapping("test4")
    public String test4() {
        Order order = new Order();
        // 库存异常
        order.setCount(15);
        // 账户金额异常
        order.setMoney(new BigDecimal(200));
        order.setProductId(1L);
        order.setStatus(1);
        order.setUserId(1L);
        LOGGER.info(" test {} ", order);
        orderServiceImpl.create(order);
        return "Create order success";
    }

    /**
     * 创建订单
     *
     * @param order
     * @return
     */
    @GetMapping("create")
    public String create(Order order) {
        orderServiceImpl.create(order);
        return "Create order success";
    }

    /**
     * 修改订单状态
     *
     * @param userId
     * @param money
     * @param status
     * @return
     */
    @RequestMapping("update")
    String update(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money, @RequestParam("status") Integer status) {
        orderServiceImpl.update(userId, money, status);
        return "订单状态修改成功";
    }

    @GetMapping("updateCount")
    String updateCount(@RequestParam("count") Integer count) {
        orderServiceImpl.changeCount(count);
        return "更新成功";
    }
}
