package com.deep.seata.standalone.service;

import com.deep.seata.standalone.mapper.ProductMapper;
import com.deep.seata.standalone.mode.Product;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author dell
 * @create 2022/6/23 16:22
 */
@SpringBootTest
public class ProductServiceTest {
    @Autowired
    private ProductMapper productMapper;

    @Test
    public void reduceStock() {
        Integer productId = 1;
        // 检查库存
        Product product = productMapper.selectByPrimaryKey(productId);
        System.out.println(product);
        assertNotEquals(product, null);
    }
}