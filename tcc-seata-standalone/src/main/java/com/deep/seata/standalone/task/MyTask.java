package com.deep.seata.standalone.task;

import com.deep.seata.standalone.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;

public class MyTask implements Runnable {
    private int taskNum;

    @Autowired
    OrderService orderService;

    public MyTask(int num) {
        this.taskNum = num;
    }

    @Override
    public void run() {
        System.out.println("正在执行task " + taskNum);
        try {
            Thread.currentThread().sleep(4000);
            orderService.createOrder(1, 1);

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("task " + taskNum + "执行完毕");
    }
}