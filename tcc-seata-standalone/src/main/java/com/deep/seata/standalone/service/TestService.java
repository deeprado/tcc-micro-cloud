package com.deep.seata.standalone.service;

import io.seata.rm.tcc.api.BusinessActionContext;
import io.seata.rm.tcc.api.BusinessActionContextParameter;
import io.seata.rm.tcc.api.LocalTCC;
import io.seata.rm.tcc.api.TwoPhaseBusinessAction;

import java.util.Map;

/**
 * @author dell
 * @create 2022/6/23 16:48
 */

@LocalTCC
public interface TestService {

    @TwoPhaseBusinessAction(name = "insert", commitMethod = "commitTcc", rollbackMethod = "cancel")
    Integer createOrder(
            @BusinessActionContextParameter(paramName = "params") Map<String, Integer> params
    ) throws Exception;

    /**
     *
     * 二阶段提交方法
     *
     * @param context 上下文
     * @return boolean
     */
    boolean commitTcc(BusinessActionContext context);

    /**
     * 二阶段取消方法
     *
     * @param context 上下文
     * @return boolean
     */
    boolean cancel(BusinessActionContext context);

}
