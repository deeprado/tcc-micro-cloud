package com.deep.seata.standalone.service;

public interface OrderService {

    /**
     * 下订单
     *
     * @param userId 用户id
     * @param productId 产品id
     * @return 订单id
     * @throws Exception 创建订单失败，抛出异常
     */
    Integer createOrder(Integer userId, Integer productId) throws Exception;

    /**
     * 1个异常
     * @param userId
     * @param productId
     * @return
     * @throws Exception
     */
    Integer createOrder1(Integer userId, Integer productId) throws Exception;

    /**
     * 2个异常
     * @param userId
     * @param productId
     * @return
     * @throws Exception
     */
    Integer createOrder2(Integer userId, Integer productId) throws Exception;

    /**
     *
     * @param userId
     * @param productId
     * @return
     * @throws Exception
     */
    Integer createOrder3(Integer userId, Integer productId) throws Exception;

    /**
     * 超时
     * @param userId
     * @param productId
     * @return
     * @throws Exception
     */
    Integer createOrder4(Integer userId, Integer productId) throws Exception;

    /**
     * 多个订单 线程池
     * @param userId
     * @param productId
     * @return
     * @throws Exception
     */
    Integer createOrder5(Integer userId, Integer productId) throws Exception;

}