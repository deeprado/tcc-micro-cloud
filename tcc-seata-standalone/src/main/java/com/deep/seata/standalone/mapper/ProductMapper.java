package com.deep.seata.standalone.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.deep.seata.standalone.mode.Product;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
@DS(value = "product-ds")
public interface ProductMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);

    int reduceStock(@Param("productId") Integer productId, @Param("amount") Integer amount);
}