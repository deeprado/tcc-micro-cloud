package com.deep.seata.standalone;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * etcd apollo 是非微服务架构，不用该注解
 */
//@EnableDiscoveryClient
//@EnableApolloConfig
@SpringBootApplication
public class SeataDistributedTransactionApplication {
//    io.seata.config.ConfigFuture
//    io.seata.spring.boot.autoconfigure.properties.config.ConfigConsulProperties
//    com.ctrip.framework.apollo.internals.RemoteConfigRepository
    public static void main(String[] args) {
        SpringApplication.run(SeataDistributedTransactionApplication.class, args);
    }

 }
