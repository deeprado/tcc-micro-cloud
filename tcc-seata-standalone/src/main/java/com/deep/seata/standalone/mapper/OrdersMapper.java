package com.deep.seata.standalone.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.deep.seata.standalone.mode.Orders;
import org.apache.ibatis.annotations.Mapper;

@Mapper
@DS(value = "order-ds")
public interface OrdersMapper {

    int deleteByPrimaryKey(Integer id);

    int insert(Orders record);

    int insertSelective(Orders record);

    Orders selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Orders record);

    int updateByPrimaryKey(Orders record);
}