package com.deep.seata.standalone.contorller;

import com.deep.seata.standalone.mapper.OrdersMapper;
import com.deep.seata.standalone.mode.Orders;
import com.deep.seata.standalone.service.OrderService;
import com.deep.seata.standalone.service.TestService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Slf4j //lombok
@RestController
public class OrderController {
    private static Logger logger = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService orderService;

    @Autowired
    private TestService testService;

    @Autowired
    private OrdersMapper ordersMapper;

    @RequestMapping("/order")
    @GlobalTransactional
    public Integer createOrder(@RequestParam("userId") Integer userId,
                               @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);
        Integer res = orderService.createOrder(userId, productId);
        log.info("请求下单, 结果:{}", res);
        return res;
    }

    @RequestMapping("/order1")
    public Integer createOrder1(@RequestParam("userId") Integer userId,
                                @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);

        return orderService.createOrder1(userId, productId);
    }

    @RequestMapping("/order2")
    public Integer createOrder2(@RequestParam("userId") Integer userId,
                                @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);

        return orderService.createOrder2(userId, productId);
    }

    @RequestMapping("/order3")
    public Integer createOrder3(@RequestParam("userId") Integer userId,
                                @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);

        return orderService.createOrder3(userId, productId);
    }

    @RequestMapping("/order4")
    public Integer createOrder4(@RequestParam("userId") Integer userId,
                                @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);

        return orderService.createOrder4(userId, productId);
    }

    @RequestMapping("/order5")
    public Integer createOrder5(@RequestParam("userId") Integer userId,
                                @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);

        return orderService.createOrder5(userId, productId);
    }

    @RequestMapping("/order6")
    @GlobalTransactional
    public Integer createOrder6(@RequestParam("userId") Integer userId,
                               @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);
        Integer res = orderService.createOrder(userId, productId);
        log.info("请求下单, 结果:{}", res);
        int b = 1/0;
        return res;
    }

    @RequestMapping("/order7")
//    @Transactional
    public Integer createOrder7(@RequestParam("userId") Integer userId,
                                @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);
        Map<String, Integer> params = new HashMap<>();
        params.put("userId", userId);
        params.put("productId", productId);

        Integer res = testService.createOrder(params);
        log.info("请求下单, 结果:{}", res);
        int b = 1/0;
        return res;
    }

    @RequestMapping("/order8")
    public Integer createOrder8(@RequestParam("userId") Integer userId,
                                @RequestParam("productId") Integer productId) throws Exception {

        log.info("请求下单, 用户:{}, 商品:{}", userId, productId);
        Orders order = new Orders();
        order.setPayAmount(new BigDecimal(10));
        order.setUserId(1);
        order.setProductId(1);
        ordersMapper.insertSelective(order);

        Integer res = 1;
        return res;
    }
}