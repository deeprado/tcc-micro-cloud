package com.deep.docker.gateway.mapper;

import com.deep.docker.gateway.model.User;
import org.apache.ibatis.annotations.Param;

public interface UserMapper {

    User findByUsername(@Param("username") String username);
}
