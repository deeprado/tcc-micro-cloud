package com.deep.docker.gateway.constant;

/**
 * @author Joetao
 * @time 2021/1/27 3:38 下午
 * @Email cutesimba@163.com
 */
public interface SecurityConstant {
    String ROLE_ANONYMOUS = "ROLE_ANONYMOUS";

    String ANONYMOUS_USER = "anonymousUser";
}
