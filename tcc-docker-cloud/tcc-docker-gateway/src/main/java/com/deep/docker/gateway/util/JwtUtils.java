package com.deep.docker.gateway.util;

import com.deep.docker.gateway.config.JwtProperties;
import com.deep.docker.gateway.entity.UserDetail;
import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Joetao
 * @time 2021/1/25 2:58 下午
 * @Email cutesimba@163.com
 */
@Component
@Slf4j
public class JwtUtils {
    private static final String CLAIM_KEY_USER_ID = "userId";
    private static final String CLAIM_KEY_AUTHORITIES = "authorities";
    private static final String CLAIM_KEY_NICKNAME = "nickName";
    private final Map<Long, String> tokenMap = new HashMap<>();
    private final Map<Long, String> refreshTokenMap = new HashMap<>();

    @Autowired
    private JwtProperties jwtProperties;

//    @Value("${jwt.secret}")
//    private String secret;
//
//    @Value("${jwt.access_token_expiration}")
//    private Long accessTokenExpiration;
//
//    @Value("${jwt.refresh_token_expiration}")
//    private Long refreshTokenExpiration;

    private final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS256;

    public UserDetail getUserDetailFromToken(String token) {
        final Claims claims = getClaimsFromToken(token);
        if (claims == null) {
            return null;
        }
        String username = claims.getSubject();
        String userId = claims.get(CLAIM_KEY_USER_ID).toString();
        String authorities = claims.get(CLAIM_KEY_AUTHORITIES).toString();
        String nickname = claims.get(CLAIM_KEY_NICKNAME).toString();
        Set<GrantedAuthority> grantedAuthorities = Arrays.stream(authorities.split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toSet());
        return UserDetail.builder()
                .id(Long.parseLong(userId))
                .username(username)
                .authorities(grantedAuthorities)
                .nickname(nickname)
                .build();
    }

    public UserDetail getUserDetailFromAuthContext() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (UserDetail) authentication.getPrincipal();
    }

    public String generateAccessToken(UserDetail userDetail) {
        String token = generateToken(userDetail, jwtProperties.getAccessTokenExpiration());
        tokenMap.put(userDetail.getId(), token);
        return token;
    }

    public String generateRefreshToken(UserDetail userDetail) {
        String refreshToken = generateToken(userDetail, jwtProperties.getRefreshTokenExpiration());
        refreshTokenMap.put(userDetail.getId(), refreshToken);
        return refreshToken;
    }

    @Cacheable(key = "#md5UsernameAccessToken")
    public String cacheAccessToken(String md5UsernameAccessToken, String token) {
        //
        log.info("cacheAccessToken {} ", md5UsernameAccessToken);
        return token;
    }

    @Cacheable(key = "#md5UsernameRefreshToken")
    public String cacheRefreshToken(String md5UsernameRefreshToken, String refreshToken) {
        log.info("cacheAccessToken {} ", md5UsernameRefreshToken);
        return refreshToken;
    }

    public Date getExpirationDateFromToken(String token) {
        Date expiration;
        try {
            final Claims claims = getClaimsFromToken(token);
            expiration = claims.getExpiration();
        } catch (Exception e) {
            expiration = null;
        }
        return expiration;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(jwtProperties.getSecret())
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    private Date generateExpirationDate(long expiration) {
        return new Date(System.currentTimeMillis() + expiration * 1000);
    }

    public Boolean isAccessTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        if (expiration == null) {
            return true;
        }
        return expiration.before(new Date());
    }

    public Boolean isRefreshTokenExpired(String refreshToken) {
        Date expiration;
        try {
            expiration = Jwts.parser().setSigningKey(jwtProperties.getSecret()).parseClaimsJws(refreshToken).getBody().getExpiration();
        } catch (ExpiredJwtException e) {
            return true;
        }
        return expiration.before(new Date());
    }

    public String refreshToken(String refreshToken) {
        if (isRefreshTokenExpired(refreshToken)) {
            return null;
        }
        String token;
        synchronized (this) {
            final UserDetail userDetail = getUserDetailFromToken(refreshToken);
            if (userDetail == null) {
                return null;
            }
            Long userId = userDetail.getId();
            token = tokenMap.get(userId);
            if (isAccessTokenExpired(token)) {
                token = generateAccessToken(userDetail);
                tokenMap.put(userId, token);
            }
        }
        return token;
    }

    public boolean checkValidToken(String token, Long userId) {
        return tokenMap.containsKey(userId) && tokenMap.get(userId).equals(token);
    }

    public boolean removeToken(Long id) {
        return tokenMap.remove(id) != null && refreshTokenMap.remove(id) != null;
    }

    private Map<String, Object> generateClaims(UserDetail userDetail) {
        Map<String, Object> claims = new HashMap<>(8);
        claims.put(CLAIM_KEY_USER_ID, String.valueOf(userDetail.getId()));
        claims.put(CLAIM_KEY_AUTHORITIES, userDetail.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(",")));
        claims.put(CLAIM_KEY_NICKNAME, userDetail.getNickname());
        return claims;
    }

    private String generateToken(UserDetail userDetail, long expiration) {
        String username = userDetail.getUsername();
        log.info(" username = {}", username);

        String userId = String.valueOf(userDetail.getId());
        log.info(" claims = {}", userId);

        Map<String, Object> claims = generateClaims(userDetail);
        log.info(" claims = {}", claims);

        return Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .setId(userId)
                .setIssuedAt(new Date())
                .setExpiration(generateExpirationDate(expiration))
                .compressWith(CompressionCodecs.DEFLATE)
                .signWith(SIGNATURE_ALGORITHM, jwtProperties.getSecret())
                .compact();
    }
}
