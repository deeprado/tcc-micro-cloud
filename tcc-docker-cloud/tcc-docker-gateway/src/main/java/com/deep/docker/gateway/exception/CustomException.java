package com.deep.docker.gateway.exception;


import com.deep.docker.gateway.entity.ResultJson;
import lombok.Getter;

/**
 * @author Joetao
 * Created at 2018/8/24.
 */
@Getter
public class CustomException extends RuntimeException{
    private final ResultJson<?> resultJson;

    public CustomException(ResultJson<?> resultJson) {
        this.resultJson = resultJson;
    }
}
