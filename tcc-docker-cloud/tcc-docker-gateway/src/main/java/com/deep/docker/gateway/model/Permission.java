package com.deep.docker.gateway.model;

import java.util.Date;

public class Permission {

    private Integer id;

    private String name;

    private String path;

    private String method;
    private Date gmtCreate;
    private Date gmtCodified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtCodified() {
        return gmtCodified;
    }

    public void setGmtCodified(Date gmtCodified) {
        this.gmtCodified = gmtCodified;
    }
}
