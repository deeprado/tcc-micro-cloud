package com.deep.docker.gateway.constant;

public interface RedisConstant {

    String TOKEN_TEMPLATE = "token_%s";

    String PREFIX_APP = "tcc_docker_app_";

    String PREFIX_WEB = "tcc_docker_web_";
}