package com.deep.docker.gateway.service;

import com.deep.docker.gateway.entity.dto.PermissionDTO;

public interface PermissionService {
    Long addPermission(PermissionDTO permissionDTO);
}
