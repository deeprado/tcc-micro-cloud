package com.deep.docker.gateway.controller;

import com.deep.docker.gateway.mapper.UserMapper;
import com.deep.docker.gateway.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {

    @Autowired
    UserMapper userMapper;

    @Autowired
    UserService userService;

    @GetMapping("/")
    public String home() {
        return " this is gateway 1.0.3";
    }

    @GetMapping("/health")
    public String health() {
        return "this is gateway health";
    }

    @GetMapping("/hostname")
    public String hostname() {
        return "hostname";
    }

    @GetMapping("/test")
    public Object test() {
        return userMapper.findByUsername("admin");
    }

    @GetMapping("/test2")
    public Object test2() {
        return userService.findByUsername("admin");
    }

}
