package com.deep.docker.gateway.auth;

import com.deep.docker.gateway.constant.SecurityConstant;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

/**
 * @author: joetao
 * Date: 2021/1/26
 */
@Component
public class MyAccessDecisionManager implements AccessDecisionManager {

    @Override
    public void decide(Authentication authentication, Object o, Collection<ConfigAttribute> collection) throws AccessDeniedException, InsufficientAuthenticationException {
        if (collection == null) {
            return;
        }
        // authentication在未登录的情况下为匿名用户，登录状态下为用户权限
        // collection为MetadataSource查询数据库获得的访问该资源的权限
        for (ConfigAttribute configAttribute : collection) {
            String roleName = configAttribute.getAttribute();
            // 匿名
//            if (roleName == null || roleName.equals(SecurityConstant.ROLE_ANONYMOUS) ) {
//                return;
//            }
            for (GrantedAuthority ga : authentication.getAuthorities()) {
                if (roleName != null && roleName.equals(ga.getAuthority())) {
                    return;
                }
            }
        }
        throw new AccessDeniedException("没有访问权限");
    }

    @Override
    public boolean supports(ConfigAttribute configAttribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
