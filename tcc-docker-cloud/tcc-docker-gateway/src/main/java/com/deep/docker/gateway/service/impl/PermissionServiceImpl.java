package com.deep.docker.gateway.service.impl;

import com.deep.docker.gateway.entity.dto.PermissionDTO;
import com.deep.docker.gateway.repo.IPermissionDao;
import com.deep.docker.gateway.repo.PermissionDO;
import com.deep.docker.gateway.service.PermissionService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl implements PermissionService {
    private final IPermissionDao permissionDao;

    public PermissionServiceImpl(IPermissionDao permissionDao) {
        this.permissionDao = permissionDao;
    }

    @Override
    public Long addPermission(PermissionDTO permissionDTO) {
        PermissionDO permissionDO = new PermissionDO();
        BeanUtils.copyProperties(permissionDTO, permissionDO);
        permissionDO = permissionDao.save(permissionDO);
        return permissionDO.getId();
    }
}
