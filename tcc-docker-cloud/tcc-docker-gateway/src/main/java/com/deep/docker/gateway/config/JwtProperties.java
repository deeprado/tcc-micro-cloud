package com.deep.docker.gateway.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties("jwt")
public class JwtProperties {

    private String secret;
    private String url;
    /** Request Headers ： Authorization */
    private String accessTokenHeader;

    private String refreshTokenHeader;
    /** 令牌前缀，最后留个空格 Bearer */
    private String prefix;
    /** 令牌过期时间 此处单位/毫秒 */
    private Integer accessTokenExpiration;
    //
    private Integer refreshTokenExpiration;

    private String language;
}