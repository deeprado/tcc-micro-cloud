package com.deep.docker.gateway.controller;


import com.deep.docker.gateway.entity.ResultCode;
import com.deep.docker.gateway.entity.ResultJson;
import com.deep.docker.gateway.entity.UserDetail;
import com.deep.docker.gateway.exception.CustomException;

import com.deep.docker.gateway.util.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.annotation.Resource;

@Controller
public class HomeController {


    @Resource
    JwtUtils jwtUtils;

    @Autowired
    private AuthenticationManager authenticationManager;

    @GetMapping("/home")
    public String home() {
        return "home";
    }

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

//    @GetMapping("/login")
//    public String login() {
//        return "login";
//    }
//
//    @PostMapping("/login")
//    public String dologin(String username, String password) {
//        //
//        //用户验证
//        final Authentication authentication = authenticate(username, password);
//        //存储认证信息
//        SecurityContextHolder.getContext().setAuthentication(authentication);
//        //生成token
//        final UserDetail user = (UserDetail) authentication.getPrincipal();
////        User user = (User) userDetailsService.loadUserByUsername(username);
//        final String token = jwtUtils.generateAccessToken(user);
//        //存储token
////        jwtUtils.putToken(username, token);
//        return "redirect:/hello";
//    }
//
//    private Authentication authenticate(String username, String password) {
//        try {
//            //该方法会去调用userDetailsService.loadUserByUsername()去验证用户名和密码，如果正确，则存储该用户名密码到“security 的 context中”
//            return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
//        } catch (DisabledException | BadCredentialsException e) {
//            throw new CustomException(ResultJson.failure(ResultCode.UNAUTHORIZED, e.getMessage()));
//        }
//    }
}
