package com.deep.docker.gateway.controller;

import com.deep.docker.gateway.entity.ResultJson;
import com.deep.docker.gateway.entity.dto.MenuDTO;
import com.deep.docker.gateway.entity.vo.UserMenuVO;
import com.deep.docker.gateway.service.MenuService;
import org.springframework.web.bind.annotation.*;

/**
 * @author Joetao
 * @time 2021/1/28 2:47 下午
 * @Email cutesimba@163.com
 */
@RequestMapping("/api/v1")
@RestController
public class MenuController {
    private final MenuService menuService;

    public MenuController(MenuService menuService) {
        this.menuService = menuService;
    }

    @GetMapping("/menus")
    public ResultJson<UserMenuVO> getMenus() {
        return ResultJson.ok(menuService.findUserMenu());
    }

    @PostMapping("/menus")
    public ResultJson<Long> addMenus(@RequestBody MenuDTO menuDTO) {
        Long menuId = menuService.addMenu(menuDTO);
        return ResultJson.ok(menuId);
    }
}
