package com.deep.docker.provider.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(name="tcc-docker-consumer")
public interface ProductService {

    /**
     * 查询
     * @return
     */
    @GetMapping("product/detail")
    public String getProductDetail();

}
