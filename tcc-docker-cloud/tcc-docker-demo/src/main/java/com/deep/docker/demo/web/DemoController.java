package com.deep.docker.demo.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class DemoController {


    @GetMapping("/")
    public String home() {
        return "this is docker";
    }

    @GetMapping("/health")
    public String health() {
        return "this is docker health";
    }

    @GetMapping("/timer")
    public String timer() {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "this is docker timer";
    }

}
