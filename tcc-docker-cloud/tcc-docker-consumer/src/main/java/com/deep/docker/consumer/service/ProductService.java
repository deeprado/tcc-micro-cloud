package com.deep.docker.consumer.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

@Component
@FeignClient(name="tcc-docker-provider")
public interface ProductService {

    /**
     * 查询
     * @return
     */
    @GetMapping("product/detail")
    public String getProductDetail();

}
