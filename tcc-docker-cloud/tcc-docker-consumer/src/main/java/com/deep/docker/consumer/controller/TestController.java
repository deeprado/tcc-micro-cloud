package com.deep.docker.consumer.controller;

import com.deep.docker.consumer.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController {
    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    private ProductService productService;

    @GetMapping("/")
    public String home() {
        return " this is consumer 1.0.3";
    }


    @GetMapping("/detail")
    public String detail() {
        return productService.getProductDetail();
    }

    @GetMapping("/health")
    public String health() {
        return "this is consumer health";
    }

    @GetMapping("/hostname")
    public String hostname() {
        return "hostname";
    }

    @GetMapping("/clients")
    public Object clients() {
        List<List<ServiceInstance>> servicesList = new ArrayList<>();
        // 获取服务名称
        List<String> serviceNames = discoveryClient.getServices();
        for (String serviceName : serviceNames) {
            //获取服务中的实例列表
            List<ServiceInstance> serviceInstances = discoveryClient.getInstances(serviceName);
            servicesList.add(serviceInstances);
        }
        return servicesList;

    }

    @GetMapping("/demo")
    public Object demo() {
        RestTemplate restTemplate = new RestTemplate();

        return "ok";
    }
}
