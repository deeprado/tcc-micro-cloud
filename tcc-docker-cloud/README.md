

1. host 

127.0.0.1 tcc-docker-eureka
127.0.0.1 tcc-docker-provider
127.0.0.1 tcc-docker-consumer

2. application.yml 
使用host name 进行链接


3. docker 启动 --net 通过host name 建立连接 

```shell script

docker run --name tcc-docker-eureka -p 8761:8761 --rm  deeprado/tcc-docker-eureka:1.0-SNAPSHOT 
docker run --name tcc-docker-consumer -p 10010:10010 --link tcc-docker-eureka --rm deeprado/tcc-docker-consumer:1.0-SNAPSHOT
docker run --name tcc-docker-provider -p 10020:10020 --link tcc-docker-eureka --rm deeprado/tcc-docker-provider:1.0-SNAPSHOT

```
# 这个不行

```shell script
docker run --name tcc-docker-consumer -p 10010:10010 --network host --rm deeprado/tcc-docker-consumer:1.0-SNAPSHOT
docker run --name tcc-docker-provider -p 10020:10020 --network host --rm deeprado/tcc-docker-provider:1.0-SNAPSHOT
```

//


Kubernetes 关闭服务器前停止服务
关机过程

0.node节点

[root@dn02 ~]# systemctl stop kube-proxy
[root@dn02 ~]# systemctl stop kubelet

1.master

[root@dn01 ~]# systemctl stop kube-scheduler
[root@dn01 ~]# systemctl stop kube-controller-manager
[root@dn01 ~]# systemctl stop kube-apiserver.service

2.关闭 node节点的flanneld 服务

[root@dn02 ~]# systemctl stop flanneld

3.全部节点关闭etcd

[root@dn0X~]# systemctl stop etcd

[root@dn0X ~]# systemctl stop docker

4.全部关机

[root@dn01 ~]# init 0

 

开机过程

systemctl start etcd  三节点

systemctl start flanneld （node 节点）

systemctl start kube-apiserver.service  (默认设置了开机启动， master 节点)

systemctl start kube-scheduler （默认设置了开机启动， master 节点）

systemctl start kube-controller-manager （默认设置了开机启动， master 节点）

systemctl start kubelet（node 节点）

systemctl stop kube-proxy（node 节点）