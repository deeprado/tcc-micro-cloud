package com.deep.seata.dao;

import com.deep.seata.dto.OrderDTO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 *
 */
@Mapper
public interface OrderDao {

  /**
   * 插入订单
   * @param order
   */
  @Insert({"INSERT INTO `order`(`order_no`,`user_id`, `product_id`, `count`, `money`) VALUES (#{orderNo},#{userId},#{productId},#{count},#{money})"})
  void insert(OrderDTO order);

  /**
   * 删除订单
   * @param orderNo
   */
  @Delete({"delete from `order` where order_no = #{orderNo}"})
  void delete(@Param("orderNo") String orderNo);
}
