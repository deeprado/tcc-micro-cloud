package com.deep.seata.controller;

import com.deep.seata.dto.OrderDTO;
import com.deep.seata.dto.TestDTO;
import com.deep.seata.service.TccActionOne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * tcc rm分支注册--下单服务
 */
@RestController
public class RmOneController {

  @Autowired
  private TccActionOne tccActionOne;

  @PostMapping(value = "/order/create",consumes = MediaType.APPLICATION_JSON_VALUE)
  public String createOrder(@RequestBody OrderDTO order){
    tccActionOne.createOrder(null,order);
    return "ok";
  }

  @PostMapping(value = "/order/test",consumes = MediaType.APPLICATION_JSON_VALUE)
  public String createOrder(@RequestBody TestDTO test){
    System.out.println(test);
    System.out.println("order/test");
    return "ok";
  }

}
