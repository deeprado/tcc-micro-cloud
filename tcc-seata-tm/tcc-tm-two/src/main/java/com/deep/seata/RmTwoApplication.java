package com.deep.seata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 *
 */
@EnableEurekaClient
@SpringBootApplication
public class RmTwoApplication {

  public static void main(String[] args) {
    SpringApplication.run(RmTwoApplication.class);
  }

}
