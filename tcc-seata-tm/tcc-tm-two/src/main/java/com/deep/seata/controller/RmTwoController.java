package com.deep.seata.controller;

import com.deep.seata.dto.TestDTO;
import com.deep.seata.service.StorageIncreaseService;
import com.deep.seata.service.TccActionTwo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 *
 */
@RestController
public class RmTwoController {

    @Autowired
    private TccActionTwo tccActionTwo;

    @Autowired
    private StorageIncreaseService storageIncreasePrepare;

    @GetMapping(value = "/storage/reduce/{productId}/{count}")
    public Boolean reduceStorage(@PathVariable("productId") long productId, @PathVariable("count") Integer count) {
        return tccActionTwo.storageReducePrepare(null, productId, count, false);
    }

    @GetMapping(value = "/storage/reduceError/{productId}/{count}")
    public Boolean reduceStorageError(@PathVariable("productId") long productId, @PathVariable("count") Integer count) {
        return tccActionTwo.storageReducePrepare(null, productId, count, true);
    }

    @GetMapping(value = "/storage/increase")
    public void increaseStorage() {
        storageIncreasePrepare.storageIncreasePrepare(null);
    }

    @PostMapping(value = "/storage/test", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String createOrder(@RequestBody TestDTO test) {
        System.out.println(test);
        System.out.println("storage/test");

        return "ok";
    }

}
