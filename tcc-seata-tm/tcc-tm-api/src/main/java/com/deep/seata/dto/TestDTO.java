package com.deep.seata.dto;

import java.math.BigDecimal;

/**
 *
 */
public class TestDTO {

  /**
   * 主键id
   */
  private Long id;
  /**
   * 主键id
   */
  private String title;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  @Override
  public String toString() {
    return "TestDTO{" +
            "id=" + id +
            ", title='" + title + '\'' +
            '}';
  }

}
