package com.deep.seata.feign.api;

import com.deep.seata.dto.TestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 */
@FeignClient(name = "seata-tcc-tm-two")
public interface RmTwoApi {

    @GetMapping(value = "/storage/reduce/{productId}/{count}")
    public Boolean reduceStorage(@PathVariable("productId") long productId, @PathVariable("count") Integer count);

    @GetMapping(value = "/storage/reduceError/{productId}/{count}")
    public Boolean reduceStorageError(@PathVariable("productId") long productId, @PathVariable("count") Integer count);

    @GetMapping(value = "/storage/increase")
    public void increaseStorage();

    @PostMapping(value = "/storage/test", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String testTwo(@RequestBody TestDTO test);

}
