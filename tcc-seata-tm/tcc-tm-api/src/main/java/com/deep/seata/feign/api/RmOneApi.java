package com.deep.seata.feign.api;

import com.deep.seata.dto.OrderDTO;
import com.deep.seata.dto.TestDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 *
 */
@FeignClient(value = "seata-tcc-tm-one")
public interface RmOneApi {

  @PostMapping(value = "/order/create",consumes = MediaType.APPLICATION_JSON_VALUE)
  public String createOrder(@RequestBody OrderDTO order);

  @PostMapping(value = "/order/test",consumes = MediaType.APPLICATION_JSON_VALUE)
  public String testOne(@RequestBody TestDTO test);


}
