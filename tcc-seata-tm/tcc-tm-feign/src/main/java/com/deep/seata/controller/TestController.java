package com.deep.seata.controller;

import com.deep.seata.dto.OrderDTO;
import com.deep.seata.dto.TestDTO;
import com.deep.seata.feign.api.RmTwoApi;
import com.deep.seata.feign.api.RmOneApi;
import io.seata.spring.annotation.GlobalTransactional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 *
 */
@RestController
public class TestController {

    private final static Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @Autowired
    private RmOneApi rmOneApi;

    @Autowired
    private RmTwoApi rmTwoApi;

    @GetMapping("/tm/testone")
    public String testOne(HttpServletRequest request) {
        TestDTO testDTO = new TestDTO();
        testDTO.setId(1L);
        testDTO.setTitle("tooooooooo");

        rmOneApi.testOne(testDTO);

        return "ok";
    }

    @GetMapping("/tm/testtwo")
    public String testTwo(HttpServletRequest request) {
        TestDTO testDTO = new TestDTO();
        testDTO.setId(2L);
        testDTO.setTitle("twwwwwww");
        rmTwoApi.testTwo(testDTO);
        return "ok";
    }


    @GlobalTransactional
    @PostMapping("/tm/purchase")
    public String test(HttpServletRequest request, @RequestBody OrderDTO orderDTO) {
        String orderNo = Long.valueOf(RandomUtils.nextLong()).toString();
        orderDTO.setOrderNo(orderNo);
        LOGGER.info("create order {}", orderDTO);

        String result = rmOneApi.createOrder(orderDTO);
        LOGGER.info("create order {}", result);
        String type = request.getHeader("type");
        if (StringUtils.isNotEmpty(type) && type.equalsIgnoreCase("hot")) {
            LOGGER.info("create order hot 减库存");

            // 更新操作-热点数据测试
            boolean bol = rmTwoApi.reduceStorage(orderDTO.getProductId(), orderDTO.getCount());
            if (!bol) {
                throw new RuntimeException("库存扣减失败");
            }
        } else {
            rmTwoApi.increaseStorage();
        }
        return "ok";
    }

    @GlobalTransactional
    @GetMapping("/tm/purchase2")
    public String test2(HttpServletRequest request) {
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setCount(1);
        orderDTO.setMoney(BigDecimal.valueOf(2.2));
        orderDTO.setProductId(1L);
        orderDTO.setStatus(1);
        orderDTO.setUserId(10L);

        String orderNo = Long.valueOf(RandomUtils.nextLong()).toString();
        orderDTO.setOrderNo(orderNo);
        String result = rmOneApi.createOrder(orderDTO);
        LOGGER.info("create order {}", result);
        String type = request.getHeader("type");
        if (StringUtils.isNotEmpty(type) && type.equalsIgnoreCase("hot")) {
            //更新操作-热点数据测试
            boolean bol = rmTwoApi.reduceStorage(orderDTO.getProductId(), orderDTO.getCount());
            if (!bol) {
                throw new RuntimeException("库存扣减失败");
            }
        } else {
            rmTwoApi.increaseStorage();
        }
        return "ok";
    }

    @GlobalTransactional
    @PostMapping("/tm/purchase3")
    public String test3(HttpServletRequest request, @RequestBody OrderDTO orderDTO) {
        String orderNo = Long.valueOf(RandomUtils.nextLong()).toString();
        orderDTO.setOrderNo(orderNo);
        LOGGER.info("create order {}", orderDTO);

        String result = rmOneApi.createOrder(orderDTO);
        LOGGER.info("create order {}", result);

        boolean bol = rmTwoApi.reduceStorageError(orderDTO.getProductId(), orderDTO.getCount());
        if (!bol) {
            throw new RuntimeException("库存扣减失败");
        }

        return "ok";
    }
}
