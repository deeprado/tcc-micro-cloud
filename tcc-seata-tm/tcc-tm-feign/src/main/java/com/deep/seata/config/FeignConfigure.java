package com.deep.seata.config;

import feign.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 *
 */
@Configuration
public class FeignConfigure {
    // 连接超时时间
    public static int connectTimeOutMillis = 60000;
    // 读取
    public static int readTimeOutMillis = 60000;

    @Bean
    public Request.Options options() {
        return new Request.Options(connectTimeOutMillis, TimeUnit.MILLISECONDS, readTimeOutMillis,
                TimeUnit.MILLISECONDS, true);
    }

}
